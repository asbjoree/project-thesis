## 29.10.2018
Minor changes to problem statement. Wrote a bit on introduction chapter.

Went throught the entire report and fixed some minor issues. Wrote some more on
software implementation chapter.


## 24.10.2018
Wrote more results.


## 23.10.2018
Wrote results. Tried to formulate the transformation mathematically.


## 22.10.2018
Wrote on results chapter.


## 17.10.2018
Wrote on the proposed solution chapter. Started describing the solution itself.


## 16.10.2018
Wrote a lot on software implementation.

Converted ray tracer C++ -> C.


## 15.10.2018
Meeting with Sverre. He has skimmed over my thesis so far, and is happy. Keep
doing what I'm doing.

Wrote a bit on software impl.


## 10.10.2018
Got animation in working order. Needs to be more efficient.

Started looking at adding normals to the SVO format.


## 08.10.2018
Wrote more about SVO algo.

Simple car animation up and running. Bit buggy model tho.


## 05.10.2018
Meeting with Øystein. Talked about a lot, he is happy with my progress. Keep in
mind everything you do that may be 'new' and write about it.


## 03.10.2018
Proofread detailed algorithm description for traversal of octrees, and also
started writing similar description of SVO data structure. More figures created.

## 02.10.2018
Wrote a lot on detailed algorithm description for traversal of octrees. Created
lots of figures on draw.io.

## 01.10.2018
Meeting with Sverre. Talked about amount of references (survey paper?). Also
talked about publishing a paper during Master's thesis. May help with Ph.d.

Wrote on recursive ray tracing, and also cleaned up references. Added page
numbers to large works.


## 28.09.2018
Wrote on report exclusively. Wrote background, located sources, created figures.
Also started writing the detailed algorithm description.


## 26.09.2018
Wrote on thesis for the most part. Wrote on APIs and some previous work/elected
algos.

Also looked a little bit at improving the SVO data structure (not all far ptrs).
Did not get it bug free, tho, and it is slooow. Maybe just revert?


## 25.09.2018
Meeting with Sverre. Not much to say.

Made the binvox->svo converter into its own thing. Saved svo as files, now much
quicker at startup.

Started working on animation. Got a basic thing up and running.

Wrote a bit on thesis.


## 24.09.2018
Write on report. Added some images, wrote a bit on rasterisation and ray
tracing, wrote on octree/svo.


## 19.09.2018
Got binvox parsing to work. A bit slow, so I should look at improving the
performance of the tracing algo. Try to manage without recursion?

Now works without recursion. Still a bit slow.


## 18.09.2018
Meeting with Sverre. Talked about report as well as general stuff. Started
working on binvox-parsing.


## 17.09.2018
Tried to implement svo tracing. Something now works. Looked at Revelles algo.


## 14.09.2018
Worked a bit on the report. Talked with Øystein a bit (technical), tried to
convey my thoughts and ideas. It got a bit clearer to me as well. Helpful.


## 13.09.2018
Worked on practical stuff. Solved the ray tracer inverse projection matrix
problem, and created a camera that one can move with keys/mouse.


## 11.09.2018
Report writing all day. Wrote background and started seriously writing previous
work.


## 10.09.2018
Meeting with Sverre at the start of the day. Discussed scope and talked some. He
recommended writing more background now that I have the time. Results can wait.
Discussion can wait. Not much else to say about it.

Spent the rest of the day writing background chapter. Wrote about vector spaces,
transforms, models, and a bit of CG history.


## 07.09.2018
Got CUDA to work with PBO. OpenGL reads the PBO object and copies it over to a
texture, then renders it. Very fast!

Simple geometric ray tracer written with support for multisampling and
refraction/reflection. Projection is hacky. Will look at proper projection next,
then maybe simple octree traversal.


## 03.09.2018
Meeting with Sverre. Showed him current progress with report. Good response. He
answered a couple of questions about structure, and told me the problem
statement was well written. It might change a little bit throughout the term,
but it's good for now. Project title can be changed until due date.

Ask Øyvind about new GFX hw, if not maybe buy myself? Figures can be 'stolen',
Sverre will take the heat. But you need to specify source. You should rarely use
single-word headlines. Nomenclature/acronym list can be included if it serves a
purpose.


## 31.08.2018
Started writing background chapter. Wrote about fundamental concepts of computer
graphics, history, models, and transforms.


## 30.08.2018
Got a simple GLFW demo up and running. Ready for CPU software ray tracing next.
Looked at report structure. Wrote some headings.

