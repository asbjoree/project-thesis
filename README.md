# Project Thesis
Animation of SVO for use in real-time ray tracing

## Report layout

* Title page
* Problem statement
* Abstract
* Sammendrag?
* Preface
* Contents
* Introduction
* Background
* Previous work
* Results
* Discussion
* Conclusions
* Bibliography
* Appendices

## Ideas

### Number 1
one SVO = one 'model'

Each 'model' has a transform (rotation, scale, translation) and a bounding sphere, calculated from scale and translation only:

* https://en.wikipedia.org/wiki/Bounding_sphere

Each model is placed somewhere in a tree-structure (octree, bsp, kd-tree)

**When rendering**:

* Sort models by distance to camera, using bounding sphere.
* OR, use tree structure and test all models located in current node.
* Use ray-sphere intersection to see if current ray intersects closest bounding sphere.
    * https://www.siggraph.org/education/materials/HyperGraph/raytrace/rtinter1.htm
* If so, transform co-ordinate system so the model SVO is in normalized space, and trace it. 
* If it the ray does not terminate, mark this model as visited-miss, and move onto next intersected bounding sphere.
* If the ray terminates, record the termination depth, mark the model as visited-hit (primary hit).
* For all bounding spheres which lie closer to the camera than the termination 'depth' (if any) but are un-visited, trace these as well, and compare depths if ray terminates. Pick closest one.

**Still need to figure out**:

* Optimization in bounding sphere storage. What tree-structure to use.
* Can these SVO models be scaled non-uniformly?

