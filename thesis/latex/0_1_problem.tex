\chapter{Problem statement}
\textbf{Ray tracing} is a rendering technique in which rays are cast from a
viewpoint into a scene in order to generate images. Briefly stated, it entails
tracing in reverse the paths that physical light particles would take. The
technique is viewed as an alternative to rasterisation rendering, which is the
\textit{de facto} standard in computer graphics today. The latter is based on
the rasterisation of primitives---most commonly triangles.

As a consequence of the technique's physical nature, ray tracing implementations
will often produce more realistic images, and will generally result in a more
visually and physically correct rendition of a given scene, when compared to
rasterisation renders. Caused in part by the fact that the objects to be
rendered may be modelled perfectly in a geometric sense, but also because
physical effects such as reflection, refraction, and transparency are much
easier to model and implement than for rasterisation rendering.

An \textbf{octree} is a space partitioning scheme that divides three-dimensional
space into a tree structure. It is analogous to a binary tree in three
dimensions, meaning that each node in the tree may have up to eight children. A
sparse voxel octree---abbreviated \acrshort{svo}---is a specific flavour of
octree often used in conjunction with ray tracing. What sets an \acrshort{svo}
apart from the generalised octree is that the data structure itself is employed
to natively represent volumetric data. Physical objects may be approximated by
treating the \acrshort{svo} leaf nodes as either filled or empty (void) voxels.
Several solutions have been proposed in the relevant literature for efficient
real-time rendering of \acrshort{svo}s using ray tracing.

A drawback of using \acrshort{svo}s to store and render volumetric data is that
the data is inherently static; \acrshort{svo}s by themselves do not support any
form of animation or efficient data mutation. In order to use this data
structure in the rendering of realistic real-time graphics, there is a need for
animation---most notably rotation, translation, but also general affine
transformations. This is necessary if ray tracing is to be used to render
realistic, animated worlds, and not just static scenes.

\textbf{In the project thesis}, the student will:
\begin{itemize}
    \item Review existing literature on the subject of ray tracing and sparse
        voxel octrees, and earlier attempts of animation in this context.
    \item Formulate a method that permits animation of \acrshort{svo} data for
        use in ray tracing. Evaluate the efficiency of the method in terms of
        performance and suitability for implementation in hardware.
    \item If feasible, demonstrate the method by ray tracing animated
        \acrshort{svo}s in real-time.
\end{itemize}

