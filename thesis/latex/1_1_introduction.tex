\chapter{Introduction}

\begin{quote}
    I do think that there is a very strong possibility---as we move towards
    next generation technologies---for a ray tracing architecture that uses a
    specific data structure. (...) There is a specific format I have done some
    research on that I am starting to ramp back up on for some proof of concept
    work for next generation technologies. It involves ray tracing into a
    sparse voxel octree.

    \flushright{--- \textit{John Carmack, 2008} \cite{carmack08}}
\end{quote}

It seems that in recent time, the topic of real-time ray tracing has been on
everyone's lips. In the autumn of 2018, the graphics card manufacturer Nvidia
promised to ``reinvent graphics'' with their new \textit{GeForce RTX} series
featuring hardware acceleration of ray tracing \cite{nvidiartx}. The response
to their showcase of what the technology has to offer suggests that, this time,
ray tracing has really caught the public's interest. But ray tracing is far
from a new concept, and throughout its history, the interest in the technology
has had both ups and downs.

The main issue concerning ray tracing as a technology appears to be that
real-time operation is very difficult to achieve. And, even though it may seem
that the technology has finally caught up to enable such performance, the
resistance to adoption of ray tracing compared to sticking to regular
rasterisation approaches has been too large. Ray tracing is not utilised as a
technology in real-time rendering because there exists very little hardware to
accelerate it, and there is a sparse selection of hardware because ray tracing
is not widely used.

The issue of adoption appears to be a classic example of \textit{path
dependence} \cite{roe96}, and escaping or correcting such a dependence will in
many cases require some form of disruptive innovation. However, in this case,
the solution may be an approach that has recently gained attention---using ray
tracing in conjunction with traditional rasterisation. The idea is to try to
achieve a sort of \textit{best-of-both-worlds} scenario, where the realism of
ray tracing is combined with the speed of rasterisation. This approach is a
central part of what Nvidia demonstrates with their new series of graphics
cards, and is also reinforced in newer papers written on the topic
\cite{wilhelmsen12}\cite{bautembach11}.

\subsubsection{Interpretation of problem statement}
The problem statement details the current state of affairs in the field, and
briefly explains a few relevant concepts. The task itself is split into three
main points. The first is to perform a thorough literature review in the field
and explore earlier attempts. The second point concerns the formulation of a
method for animation of sparse voxel octree data. The third point entails
implementing the method in some way. Since the problem text is written
precisely, there is not a lot of room for interpretation. However, some
choices must be made and justified.

The third point states that an implementation of the method should be
demonstrated, but does not specify what kind of demonstration should be made.
As a consequence of the limited time window, the choice is to implement the
demonstration in software. A hardware implementation would frankly result in a
project with too large a scope for a project thesis.

Another decision that must be justified is what kind of animation that will be
supported by the solution. The problem text states that animation should be
permitted, but does not define the nature of said animation. The background
text in the problem statement does, however, discuss the nature of the
animation to some degree; affine transformations are mentioned. In order to
limit the scope of the work, this thesis will focus on rigid-body animation.
This distinction is made on the basis that animation of rigid bodies is in
general easier to achieve than other forms of animation---for instance mesh
animation, which is more suited when using a rasterisation approach.

\subsubsection{Thesis outline}
In this thesis, a method for animation of sparse voxel octrees will be
presented. A brief introduction to relevant concepts can be found in
\Cref{ch:background}. The research context as a whole is discussed in
\Cref{ch:prev_work}, and the algorithms and schemes upon which this thesis is
based are detailed in \Cref{ch:elected_algo}. Starting with
\Cref{ch:proposed_method}, the results are presented. In this chapter, the
method for animation is explained and discussed. In \Cref{ch:software_demo}, an
actual implementation of this method is analysed. Finally, the thesis is
concluded in \Cref{ch:conclusions}.

\subsubsection{A small note about figures}
Unless otherwise specified, all figures presented in this text are designed and
created by the author. It is not an insignificant workload that has been laid
down in their creation, and as such, they should be regarded as original (or, in
some cases, improved derivative) works and hopefully contribute to the credit of
this thesis.
