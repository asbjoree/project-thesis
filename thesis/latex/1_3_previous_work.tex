\chapter{Research context}\label{ch:prev_work}
In this chapter, the research field as a whole will be explored. The specific
papers and algorithms upon which this thesis is based is detailed in
\Cref{ch:elected_algo}. After a thorough literature review, few attempts at
animation of sparse voxel octrees were found. However, there is a vast body of
published papers on the topic of efficient ray tracing and traversal of sparse
voxel octrees.

\section{Algorithms for octree traversal}
Since the extent of the research done on the subject is very large, only the
most prominent works, and those of the highest relevance to this thesis, will be
discussed in this section.

The earliest method for traversal of octrees found in the literature was
authored by \textcite{glassner84} in 1984. The paper states that over 95 percent
of the total rendering time may be spent on ray-object intersection
calculations. Hence, there is a huge potential for performance gain by
optimising this process. Glassner then suggests sorting the scene into an octree
and presents an algorithm for traversal of such an octree. Another method was
introduced by \textcite{levoy90}. In the paper he introduces two methods for
enhancing the performance of ray tracing of volumetric data, the first of which
employs octrees to encode spatial coherence in the data.

Many subsequent attempts at improving the performance of octree traversal exist.
They can generally be grouped into two main categories based on how they solve
the traversal problem: bottom-up and top-down schemes.  The algorithm by
\textcite{glassner84}, as well as other, similar schemes
\cite{samet89}\cite{samet90}, are instances of bottom-up octree traversal
algorithms. The method by \textcite{levoy90}, as well as the \textit{HERO
algorithm} presented by \textcite{agate89}, and a host of other algorithms
\cite{jansen86}\cite{cohen93}\cite{gargantini93}\cite{endl94}\cite{knoll06}
provide examples of a top-down parametric traversal algorithms. From the number
of papers alone, it appears that top-down traversal algorithms are most popular
in the field.

An efficient algorithm for octree traversal was presented by
\textcite{revelles00} in their 2000 paper. The article introduces a top-down
parametric method that is very well documented. After this work was published,
there seems to be few new algorithms developed that contest its speed and
simplicity. In 2006, a paper published by \textcite{knoll06} describes an
algorithm based on the work of \textcite{gargantini93}, however this algorithm
is not as well-documented as \cite{revelles00}, and seems more complicated while
not revealing any tangible performance increase. Indeed, perhaps the opposite,
as the algorithm is recursive, and therefore does not translate readily for
implementation on a \acrshort{gpu} without modification.  The algorithm by
\textcite{revelles00} is also recursive, but due to its simplicity is bound to
translate well into an iterative method. In fact, this is already demonstrated
by \textcite{wilhelmsen12}.

In conclusion, the algorithm elected for traversal of sparse voxel octrees is
\cite{revelles00}. Because the algorithm is simple and fast, and also improves
upon the performance of earlier algorithms, this algorithm will serve as the
starting point of this thesis. The algorithm will be presented in detail in
\Cref{ch:elected_algo}.

\section{Parallelising the workload}
The algorithms presented in the previous section are sequential algorithms, and
do not deliberately exploit the fact that ray tracing is highly parallelisable by
nature. There are several papers that discuss advantages of using the parallel
computing capabilities of \acrshort{gpu}s to distribute the workload. A few of
the most notable are presented in the following.

A paper that does not describe a method of ray tracing, but is still noteworthy
is \textcite{gobbetti05}. They discuss the rendering of very large surface
models using \textit{out-of-core} data management, meaning that the approach
supports data sets too large to fit in working memory. In their paper, they use
hardware acceleration in the form of a \acrshort{gpu} to parallelise the
workload of rendering the data sets, while implicitly supporting different
levels of detail. A similar method could perhaps be utilised in ray tracing
hardware to support very large, highly detailed models.

In their 2009 paper, \textcite{crassin09} propose a new approach for rendering
large volumetric data sets by ray tracing on a \acrshort{gpu}. Their impressive
result is that the system achieves interactive to real-time rendering
performance of several billion voxels. The method takes care to avoid using a
stack---and therefore no recursion---in order to increase \acrshort{gpu}
optimisation, and utilises mipmapping as a \acrshort{lod}-technique in order to
hide visual noise. The algorithm supports on-the-fly loading of chunks of data
from \acrshort{cpu} memory to \acrshort{gpu} memory whenever the ray tracer
encounters missing data, which means that the size of the models is not limited
by \acrshort{gpu} memory.

The most relevant work for this thesis is the article by \textcite{laine11}.
They use the \acrshort{gpgpu} capabilities provided by Nvidia \acrshort{cuda} to
trace \acrshort{svo}s in parallel. Also introduced through their work is a
compact \acrshort{svo} memory structure that will form the basis of the
\acrshort{svo} specification used in this thesis. The memory structure presented
in this thesis can be viewed as a simplification of the structure presented in
their paper. The data structure will be detailed in \Cref{ch:elected_algo}.

\section{Methods for animation of SVOs}
Only one significant attempt at animation of sparse voxel octrees was found in
the literature. The bachelor's thesis by \textcite{bautembach11} outlines a
general method of animating sparse voxel octrees during the rendering process.
In his paper, he describes a method for animation of \acrshort{svo}s based on
the idea that each leaf node of the tree is an individual \textit{atom} that may
be animated. The method presented can be regarded as a \textit{bottom-up}
solution to the problem of animation. Bautembach's proposition contrasts what
will be presented here in that the solution introduced in this thesis may be
viewed as a \textit{top-down} approach.

Note that Bautembach presents a general method for animation of sparse voxel
octrees, but that this method is not suitable for ray tracing. As is explicitly
stated by Bautembach himself, his method destroys the hierarchical structure of
the \acrshort{svo} to be rendered. Consequently, most ray tracing algorithms
will no longer work, since the method in turn prohibits efficient intersection
tests. In his work, he therefore resorts to rasterisation in order to render the
animated sparse voxel octrees.

\section{Other works of significance}
Another thesis of interest is a master's thesis written by
\textcite{wilhelmsen12}. In it he implements a hardware ray tracer for sparse
voxel octree data. The thesis is very well written, and many of the choices made
are relevant to this project. For instance, his choice of algorithms closely
match the algorithms selected for this thesis, and the justifications for the
choices are similar, since both this thesis and his work are attempts at tracing
\acrshort{svo}s in a manner suited for hardware acceleration.
