\chapter{Established algorithms chosen as foundation}\label{ch:elected_algo}
In this chapter a set of established algorithms and schemes that are to be used
in the thesis, will be presented. The workings of the algorithms will be
explained in detail, as well as the reasoning behind their selection.

\section{An efficient parametric algorithm for octree
traversal}\label{sec:svo_traversal}
The chosen algorithm for traversal of octrees, and in the case of this thesis,
sparse voxel octrees, is the parametric algorithm proposed by
\textcite{revelles00} in their 2000 paper. After the literature review, this
algorithm seemed the most efficient and well documented. Many newer
papers---such as works on global illumination \cite{thiedemann11}, virtual X-ray
imaging \cite{li07}, and the very relevant works of \textcite{laine11} and
\textcite{wilhelmsen12}---employ this algorithm in some shape or form in their
projects. This lends credence to the claim that the algorithm is still both
relevant and competitive.

The algorithm is a recursive, top-down algorithm with neighbour-finding. As will
be further discussed in \Cref{ch:software_demo}, recursive algorithms are
usually problematic in a \acrshort{gpgpu} context, however this algorithm can
without much effort be modified to be iterative instead of recursive. This is
demonstrated by \textcite{wilhelmsen12} in his hardware approach.

In this section, the algorithm will be reviewed like it is presented in
\cite{revelles00}, albeit with some minor modifications to the naming of
parameters and variables. In addition, the algorithm for the three-dimensional
case had to be converted to use a right-handed co-ordinate system.

\subsection{Simplified algorithm for the 2D case}
To present the algorithm in as straightforward a manner as possible, a
simplified version for traversal of two-dimensional quadtrees is considered. The
algorithm is parametric, which in this context means that all computations are
centred around a parameter $t$, such that $t$ is a positive number describing a
point $\mathbf{p}$ along a ray $R$ as shown in \Cref{eq:point_on_ray}. The ray
$R$ is defined by its origin $\mathbf{r}_o$ and direction $\mathbf{r}_d$.
\begin{equation}\label{eq:point_on_ray}
    \mathbf{p}(t) = R_t(\mathbf{r}_o, \mathbf{r}_d) = \mathbf{r}_o + t \cdot
    \mathbf{r}_d \ , \quad t \geq 0 \\
\end{equation}
For now, all rays are assumed to have directions with strictly positive
components. The algorithm will at a later stage be extended to allow rays with
arbitrary directions.

\subsubsection{First phase: node boundaries}
The first concept to be introduced is the characterisation of node boundaries.
In the two-dimensional case, each node has four boundaries. These boundaries
represent the four edges of the node, and are given the names $x_0$, $x_1$,
$y_0$, and $y_1$. Since the algorithm is parametric, the boundary of a node may
also be defined by the value $t$ at which the ray crosses the boundary.
Therefore, the following values of $t$ are declared: $t_{x0}$, $t_{x1}$,
$t_{y0}$, and $t_{y1}$. The values and how they relate graphically to the ray
$R$ can be seen in \Cref{fig:node_boundaries}

\begin{figure}[htb!]
    \centering
    \includegraphics[width=0.55\textwidth]{figures/node_boundary.pdf}
    \caption{The node boundaries.}\label{fig:node_boundaries}
\end{figure}

The first phase of the algorithm therefore consists of calculating the values of
$t$ at which the ray crosses each of the boundaries. A simple expression for
each boundary is derived in \cite{revelles00} and given in
\Cref{eq:node_boundaries}.
\begin{equation}\label{eq:node_boundaries}
    \begin{aligned}
    t_{x0} &= (x_0 - r_{ox}) / r_{dx} \quad &\land \\
    t_{x1} &= (x_1 - r_{ox}) / r_{dx} \quad &\land \\
    t_{y0} &= (y_0 - r_{oy}) / r_{dy} \quad &\land \\
    t_{y1} &= (y_1 - r_{oy}) / r_{dy}
    \end{aligned}
\end{equation}

\subsubsection{Second phase: intersection test}
Once these values of $t$ are calculated, the second phase of the algorithm
begins. The goal of this phase is to determine if the ray intersects the node at
all. Since the ray direction is assumed positive, it is evident that $t_{x0} <
t_{x1}$ and $t_{y0} < t_{y1}$. This means that if the ray intersects the node,
the boundaries $t_{x0}$ and $t_{y0}$ will be the boundaries through which the
ray enters the node, and conversely, $t_{x1}$ and $t_{y1}$ are the boundaries
through which the ray leaves the node.

It can be shown that if the maximum $t$-value of all the enter boundaries is
less than the minimum $t$-value of all the exit boundaries, the ray intersects
the node. Therefore, two more values of $t$ are calculated as shown in
\Cref{eq:t_min_max}.
\begin{equation}\label{eq:t_min_max}
    \begin{aligned}
        t_{\text{min}} &= \text{max}(t_{x0}, t_{y0}) \quad \land \\
        t_{\text{max}} &= \text{min}(t_{x1}, t_{y1})
    \end{aligned}
\end{equation}
Hence, the intersection test consists of checking the expression $t_{\text{min}}
< t_{\text{max}}$. In addition, $t_{\text{min}}$ and $t_{\text{max}}$ are the
$t$-values at which the ray enters and exits the node itself. The proof for
these statements can be found in \cite{revelles00}.

\subsubsection{Third phase: recursion}
If the intersection test passes, the algorithm moves onto the third phase. In
this phase, the algorithm will recurse into the children of the node. First, two
additional $t$-values are defined. These are the values of $t$ at which the ray
crosses the centre---or middle---of the node. The values are defined
mathematically as shown in \Cref{eq:t_middle}.
\begin{equation}\label{eq:t_middle}
    \begin{aligned}
        t_{xm} &= \left(t_{x0} + t_{x1}\right) / 2 \quad \land \\
        t_{ym} &= \left(t_{y0} + t_{y1}\right) / 2
    \end{aligned}
\end{equation}

Selecting the child nodes that are intersected by the ray is a matter of
systematically testing the different parameters introduced so far. Pseudocode
for the process is shown in \Cref{fig:child_node_selection}.

\vfill

\begin{figure}[htb!]
    \indent\makebox[\textwidth][c]{%
    \begin{minipage}{0.7\textwidth}
        \vspace{1mm}\hrule\vspace{2mm}
        \makebox[1cm]{\textbf{1.}\hfill}\textbf{if} $t_{\text{min}} <
        t_{\text{max}}$ \textbf{then}\par
        \makebox[2cm]{\textbf{2.}\hfill}\textbf{if} $t_{ym} < t_{xm}$
        \textbf{then} the ray crosses $q_2$ \textbf{and}\par
        \makebox[3cm]{\textbf{3.}\hfill}\textbf{if} $t_{x0} < t_{ym}$
        \textbf{then} the ray crosses $q_0$\par
        \makebox[3cm]{\textbf{4.}\hfill}\textbf{if} $t_{xm} < t_{y1}$
        \textbf{then} the ray crosses $q_3$\par
        \makebox[2cm]{\textbf{5.}\hfill}\textbf{else if} $t_{xm} < t_{ym}$
        \textbf{then} the ray crosses $q_1$ \textbf{and}\par
        \makebox[3cm]{\textbf{6.}\hfill}\textbf{if} $t_{y0} < t_{xm}$
        \textbf{then} the ray crosses $q_0$\par
        \makebox[3cm]{\textbf{7.}\hfill}\textbf{if} $t_{ym} < t_{x1}$
        \textbf{then} the ray crosses $q_3$\par
        \vspace{1mm}\hrule\vspace{2mm}
    \end{minipage}}
    \caption{Selecting the correct child node.}
    \label{fig:child_node_selection}
\end{figure}

\vfill

\clearpage

When the intersected child nodes are determined, the algorithm will process them
in turn, ordered by increasing indices. For each child node to process, the
algorithm will recurse, but skip directly to the third and final phase. Further,
the values of $t$ will differ from those of the parent node. The $t$-values to
use for each of the child nodes $q_0$, $q_1$, $q_2$, and $q_3$, can be defined
simply as some selection of the $t$-values of the parent. The rules can be seen
in \Cref{tab:t_val_conv}.

\vfill

\begin{table}[htb!]
    \renewcommand{\arraystretch}{1.3}
    \centering
    \caption{Which parameters to use when recursing into child node $q_i$.}
    \begin{tabular}{ccccc}
        \toprule
        Child node & New $t_{x0}$ & New $t_{y0}$ & New $t_{x1}$ & New $t_{y1}$ \\
        \midrule
        $q_0$ & $t_{x0}$ & $t_{y0}$ & $t_{xm}$ & $t_{ym}$ \\
        $q_1$ & $t_{xm}$ & $t_{y0}$ & $t_{x1}$ & $t_{ym}$ \\
        $q_2$ & $t_{x0}$ & $t_{ym}$ & $t_{xm}$ & $t_{y1}$ \\
        $q_3$ & $t_{xm}$ & $t_{ym}$ & $t_{x1}$ & $t_{y1}$ \\
        \bottomrule
    \end{tabular}\label{tab:t_val_conv}
\end{table}

\vfill

Illustrations of how the ray attributes affect the different parameters
introduced are shown in \Cref{fig:subnodes_1,fig:subnodes_2}. The figures may
also serve to reinforce the understanding of the process of selecting child
nodes.

\vfill

\begin{figure}[htb!]
    \centering
    \includegraphics[width=\textwidth]{figures/subnodes_1.pdf}
    \caption{Sub-nodes crossed when $t_{x0} > t_{y0}$.}\label{fig:subnodes_1}
\end{figure}

\vfill

\begin{figure}[htb!]
    \centering
    \includegraphics[width=\textwidth]{figures/subnodes_2.pdf}
    \caption{Sub-nodes crossed when $t_{y0} > t_{x0}$.}\label{fig:subnodes_2}
\end{figure}
% $

\newpage

\subsection{Extending the algorithm to octrees}
In order to extend the algorithm to traverse octrees and not quadtrees, a third
co-ordinate is introduced where needed. The parameters $t_{z0}$, $t_{z1}$, and
$t_{zm}$ are defined in a similar manner to their $x$ and $y$ counterparts in
\Cref{eq:node_boundaries,eq:t_middle}. There is also a need to redefine
$t_{\text{min}}$ and $t_{\text{max}}$. These must now be defined as shown in
\Cref{eq:t_min_max_3d}.
\begin{equation}\label{eq:t_min_max_3d}
    \begin{aligned}
        t_{\text{min}} &= \text{max}(t_{x0}, t_{y0}, t_{z0}) \quad \land \\
        t_{\text{max}} &= \text{min}(t_{x1}, t_{y1}, t_{z1})
    \end{aligned}
\end{equation}

The check that determines whether the ray intersects the node or not is
unchanged from the two-dimensional case; a simple test of $t_{\text{min}} <
t_{\text{max}}$ is evaluated. This means that---apart from introducing the third
co-ordinate---the first and second phase of the algorithm are unchanged. The
third phase, however, will differ somewhat.

\subsubsection{Obtaining the initial child node}
In the event of a successful intersection test, the process of selecting the
first child node crossed by the ray becomes more involved compared to the
quadtree case described by pseudocode in \Cref{fig:child_node_selection}.  The
first step is to obtain the entry boundary of the octree node. This boundary is
a plane in three-dimensional space, and is determined by retrieving the maximum
value among $t_{x0}$, $t_{y0}$, and $t_{z0}$. Next, a set of tests are
performed. These tests have the potential to assert a bit in a variable which is
subsequently used to index the child node.  The whole process of determining the
index of the child node is described in \Cref{tab:first_node}.

\begin{table}[htb!]
    \renewcommand{\arraystretch}{1.3}
    \centering
    \caption{Determining index of initial child node.}
    \begin{tabular}{cccc}
        \toprule
        Maximum value & Entry boundary & Conditions to examine & Index bit affected \\
        \midrule
        $t_{x0}$ & $YZ$-plane & $t_{ym} < t_{x0}$ & 1 \\
        & & $t_{zm} < t_{x0}$ & 2 \\
        \midrule
        $t_{y0}$ & $XZ$-plane & $t_{xm} < t_{y0}$ & 0 \\
        & & $t_{zm} < t_{y0}$ & 2 \\
        \midrule
        $t_{z0}$ & $XY$-plane & $t_{xm} < t_{z0}$ & 0 \\
        & & $t_{ym} < t_{z0}$ & 1 \\
        \bottomrule
    \end{tabular}\label{tab:first_node}
\end{table}

\subsubsection{Obtaining the next child node}
After traversing into the initial child node, there needs to be some
functionality for the obtaining of the next sibling node. For instance, if the
ray crosses into the first child node of the root node, but does not terminate
within this node---i.e. it hits nothing---the algorithm needs to continue the
traversal by entering the next direct child node of the root node intersected by
the ray.

The algorithm implements this functionality by defining a simple state machine
with a set of transitions. Each child node of the current parent node is a
state, and the traversal through the set of child nodes is described by
transitions. These states, as well as the allowed transitions between them, are
shown in \Cref{fig:traversal_states}. In the illustration, the index of the
states correspond to the index calculated in the previous step.

\begin{figure}[htb!]
    \centering
    \includegraphics[width=0.7\textwidth]{figures/traversal_states.pdf}
    \caption{The traversal state machine.}\label{fig:traversal_states}
\end{figure}

The rules for determining the next node to visit can be summarised as shown in
\Cref{tab:next_node}. In the table, the exit plane is determined in the same
manner as the entry plane (in \Cref{tab:first_node}), apart from using the exit
boundaries of the current child instead. The transition to the next state in the
state machine is then established based on the exit boundary and current state.
If the END state is reached, all relevant child nodes of the current parent
has been traversed.

\begin{table}[htb!]
    \renewcommand{\arraystretch}{1.3}
    \centering
    \caption{Determining next child node.}
    \begin{tabular}{cccc}
        \toprule
        Current & If $t_{x1}$ is max, & If $t_{y1}$ is max, & If $t_{z1}$ is
        max, \\
        child node & exit-plane $YZ$ & exit-plane $XZ$ & exit-plane $XY$ \\
        \midrule
        000 & 001 & 010 & 100 \\
        001 & END & 011 & 101 \\
        010 & 011 & END & 110 \\
        011 & END & END & 111 \\
        100 & 101 & 110 & END \\
        101 & END & 111 & END \\
        110 & 111 & END & END \\
        111 & END & END & END \\
        \bottomrule
    \end{tabular}\label{tab:next_node}
\end{table}

\subsection{Supporting arbitrary ray directions}
The algorithm presented so far requires that the components of the ray direction
be strictly positive. To allow arbitrary ray directions---i.e. positive,
negative, or zero components---some modifications must be made.

\subsubsection{Allowing zero-valued direction components}
Since the calculation of the entry and exit boundaries in
\Cref{eq:node_boundaries} includes an expression where each parameter is divided
by the components of the ray direction, issues arise whenever the ray direction
includes zero-valued components. There are two main ways to mitigate this
problem.

The first solution would be to include a special case that, whenever the
direction is used in some manner, checks if one or more components are zero.
Further, the algorithm must be modified to handle this special case. This means
that all the proposed rules for selecting first and next nodes must include
special checks, and would in turn lead to the algorithm becoming much more
complex.

The solution proposed in \cite{revelles00} is to allow infinite values, and
perform the check by simply setting the parameters to infinity if the direction
component is zero, then including a couple of checks when calculating the
$t_m$-values. This solves the problem mathematically, but may still lead to
issues in implementation, mostly because infinities can be hard to model in
software. A slight modification to the proposed solution which translates well
into software is discussed in \Cref{ch:software_demo}.

\subsubsection{Allowing negative direction components}
The algorithm currently only supports non-negative directions. This is ingrained
in the rules for choosing the first and subsequent child nodes. Fortunately,
\textcite{revelles00} have an elegant solution to the problem. A few
modifications are made during the initialisation phase of the algorithm, leaving
the rest of the algorithm unchanged.

Firstly, if a given component of the direction is negative, the direction and
origin of the ray are flipped around the centre of the root node for this
component. This is formulated mathematically in the following fashion: for
every negative component $i$ of the ray direction, the ray is modified as shown
in \Cref{eq:neg_dir}, where $c_i$ is component $i$ of the centre of the root
node.
\begin{equation}\label{eq:neg_dir}
    \begin{aligned}
        r_{di} &= -r_{di} \\
        r_{oi} &= c_i - r_{oi}
    \end{aligned}
\end{equation}

Secondly, the order in which child nodes are traversed must be modified to
account for the modified ray direction. A new coefficient $a$ is defined such
that after the next node index $i$ has been calculated, $i$ is modified as shown
in \Cref{eq:dir_coef}.
\begin{equation}\label{eq:dir_coef}
    \begin{aligned}
        i' &= a \oplus i \\
        a &= 4s_z + 2s_y + s_x
    \end{aligned}
\end{equation}
The operator $\oplus$ is a bitwise XOR, and the values $s_i$ are set to 1 if the
original ray direction is negative for component $i$, and 0 otherwise.

\section{Efficient sparse voxel octrees}\label{sec:svo_structure}
The sparse voxel octree data structure itself also deserves to be a topic of
discussion. The chosen data structure for this thesis is a simplified version of
the scheme presented by \textcite{laine11} in 2011. The focus of said paper was
to devise an efficient data structure for storing of voxel data. The resulting
scheme was also designed to minimise memory bandwidth requirements and therefore
has a modest memory footprint.

In this thesis, the data structure will be presented with certain
simplifications. These simplifications will be justified in the following, and
stem from the fact that some specific features of the original data structure
are unneeded, and would only increase complexity without yielding any benefits.
Many of the simplifications draw inspiration from the implementation found in
the master's thesis by \textcite{wilhelmsen12}.

\subsection{Scheme overview}
The data structure described by the scheme is in essence a large table of node
descriptors. Each entry in the table corresponds to a specific node in the tree,
and stores information about its children. This means that leaf nodes do not
have their own entry, as all information is stored in their parent. In order to
support large models while reducing memory bandwidth requirements, the data
structure is split into \textit{blocks} of contiguous memory. Within a single
such block, all memory references are relative.

\subsubsection{Node descriptor}
The basic node descriptor outlined in the paper is 64 bits wide, consisting of a
15-bit pointer field, a single-bit field describing the nature of the pointer
field, two eight-bit fields that hold the metadata about the children of the
current node, and 32 bits of contour information. However, the contour
information stored in the descriptor is not of interest in this thesis, so a
simplified version omitting this data is used. Hence, the descriptor employed in
this thesis is 32-bit wide, its layout on the form shown in
\Cref{fig:node_entry}.

\begin{figure}[htb!]
    \centering
    \includegraphics[width=0.7\textwidth]{figures/node_entry.pdf}
    \caption{A single node entry.}\label{fig:node_entry}
\end{figure}

Each of the fields shown in \Cref{fig:node_entry} serves a specific purpose.
Starting with the right-most fields: the \textit{valid mask} and \textit{leaf
mask} are bit masks that describe the children of the current node.  The masks
each have eight entries (bits) that describe each of the eight child slots by
the rules stated in \Cref{tab:mask_rules}. Next, the 15-bit field---the
\textit{child pointer}---generally stores a pointer to the entry of the first
child of the current node. The rest of the children are stored sequentially
after the first child. However, if the single-bit \textit{is far} field is
asserted, the child pointer is an indirect pointer to a \textit{far pointer}.
This means that the child pointer field will, instead of pointing directly to
the first child, point to a 32-bit pointer entry that holds a relative pointer
to the child entry. The far pointer is utilised whenever the child pointer field
is too small to hold the pointer, and will split the tree data structure into
separate, contiguous blocks of memory.

\begin{table}[htb!]
    \renewcommand{\arraystretch}{1.3}
    \centering
    \caption{Bit mask interpretation for child slots.}
    \begin{tabular}{ccl}
        \toprule
        Valid mask & Leaf mask & \multicolumn{1}{c}{Interpretation} \\
        \midrule
        0 & 0 & The child slot is empty. \\
        1 & 0 & The child slot is not a leaf and has data. \\
        1 & 1 & The child slot is a leaf, and is filled. \\
        0 & 1 & Invalid combination. \\
        \bottomrule
    \end{tabular}\label{tab:mask_rules}
\end{table}

\subsubsection{Page headers and info sections}
The scheme presented by \textcite{laine11} also includes a set of special
descriptors that will not be utilised in this thesis. As a simplification of the
voxel data structure, they will be omitted from the implementation. This
decision draws inspiration from \textcite{wilhelmsen12}, and generally results
in a simpler layout as well as a smaller memory footprint. However, since they
are part of the original scheme, they will be presented in this section.

The first omitted descriptor is the \textit{info section}. This type of entry
contains a pointer to the first node descriptor, and describes the information
available in the octree structure. In other words, it contains metadata about
the child descriptors, and states which features are in use; for instance if
contour information, colours, or normals are employed. Since none of these
features are used, the info section is not implemented at all. The second
omitted descriptor, the \textit{page header}, is spread among the child
descriptors at a set spacing. These descriptors contain a relative pointer to
the current memory block's info section. Since the info section itself is not
implemented, it stands to reason that the page header is unneeded.

\subsubsection{Example voxel data structure}
An example voxel data structure is shown in \Cref{fig:entry_table}. In the
hierarchical illustration, grey nodes are empty, blue nodes are filled, and
nodes highlighted yellow are non-terminal, meaning that they have children with
data. In \Cref{fig:entry_table_rendered}, the same entry table is rendered as an
SVO.

\begin{figure}[htb!]
    \centering
    \includegraphics[width=\textwidth]{figures/entry_table.pdf}
    \caption{An example \acrshort{svo}. Both the structure in memory and
    hierarchical layout are shown.}\label{fig:entry_table}
\end{figure}

\begin{figure}[htb!]
    \centering
    \includegraphics[width=0.7\textwidth]{figures/svo_ex.png}
    \caption{The octree specified in \Cref{fig:entry_table}
    rendered.}\label{fig:entry_table_rendered}
\end{figure}


