\chapter{The proposed method for SVO animation}\label{ch:proposed_method}
Sparse voxel octrees are static data structures \cite{laine11}. Compared to
polygonal models, which are inherently easy to manipulate, octrees often need to
be extensively modified, perhaps even restructured from the ground up, to
respond to even minor changes. As an example, animation of a polygonal model may
entail nothing more than translating a single vertex of the model by some
distance. This operation is computationally cheap, and will result in a
deformed, and hence animated, model. As for a sparse voxel octree, in order to
properly animate the volumetric data, one would need to traverse the tree and
process every voxel that should be altered as a result of the animation. In
addition, the octree structure will in most cases need to be rebuilt as a result
of the animation---some leaf nodes might be merged and substituted by a larger
leaf node, while other leaf nodes may have to be split into smaller nodes.
Compared to polygonal animation, this operation is computationally expensive,
and not suited for real-time applications.

The method of \acrshort{svo} animation discussed above results in a scheme where
each frame of a given animation sequence in essence produces a distinct sparse
voxel octree. This naïve method may be described as the bottom line, or
\textit{basis method}. In the following sections, the basis method is referred
to in order to establish a comparative foundation for a new method.

\section{Objectives}
The purpose of this thesis is to formulate an efficient method for animation
of sparse voxel octrees, that may be more relevant in a real-time setting than
the basis method described above. The problem statement requires the solution to
be efficient, but leaves open for interpretation the metrics by which the
efficiency should be measured. In order to properly measure the efficiency---and
thus validity---of a proposed method, three goals, or objectives, are formulated
and discussed in the following.

\subsubsection{Execution time}
A proposed solution should yield an improvement upon the basis method of
animation in terms of execution time. This improvement should be to such a
degree that the solution may be suitable for real-time applications.

\subsubsection{Memory requirements}
Compared to the basis method, a solution should not require any significant
increase in memory usage. It should neither present a considerable increase in
memory bandwidth requirements.

\subsubsection{Hardware suitability}
In order to provide any discernable benefits for future work, a solution must
be applicable in a hardware---or low-level software---setting. This means that
special considerations should be made in the design phase to ensure that a
solution is suited for implementation in hardware.

\section{The method}
The proposed method is now to be detailed. It takes advantage of the fact that a
rigid-body animation may be modelled as a system of rigid bodies transformed
relative to each other. This system of rigid bodies is essentially a set of
static models wherein each model has an associated transform. The main idea
is that the process of animation then consists of altering the transform
associated with each model, and leaving the model data itself unchanged. For a
polygonal model, this approach is common. The transform is applied to each and
every vertex contained in the model during the rendering phase. The next section
will discuss the possibility for adapting this method for use in ray tracing of
voxel models.

\subsection{Adapting the method for ray tracing}
As touched upon, the method discussed above is an established technique when
animating polygonal models in a rasterisation setting. Nonetheless, the
approach may be applicable in a ray tracing context as well. In ray tracing of
sparse voxel octrees, a sparse voxel octree can be regarded as a self-contained
volumetric model. As such, a rigid-body animation may be formulated as a set of
independent \acrshort{svo}s, where each \acrshort{svo} is a static, rigid body
model with a corresponding transform. The process of animation is then reduced
to simply modifying these transforms in a timely manner. The internal data of
each \acrshort{svo} may remain unmodified for the duration of the animation.

This description raises the question of how to utilise the transform in the
process of animating the volumetric data. It is not feasible to apply the
transformation to every data point contained in the model as one would do when
animating polygonal models. The solution presented here is to invert the
problem. Instead of transforming the model data, one may perform an inverse
transformation on the ray. In other words, each ray in the ray tracing process
may be transformed from world space to the local co-ordinate system of each of
the animated \acrshort{svo}s that are to be traced.

Shown in \Cref{fig:svo_ray_transform} is the procedure of transforming rays as a
part of the animation process. When a ray enters the boundary of a transformed
\acrshort{svo}, the ray itself is transformed inversely in order to facilitate
the transformation of the model. To reiterate, in place of transforming the
\acrshort{svo} data itself---which may involve transforming billions of data
points---the transformation is performed on the ray. In practice, the ray
transformation is performed by pre-multiplying the ray origin and ray direction
with a set of matrices. This matrix multiplication is a step that must be done
in any case when the ray is generated, meaning that the transformation of the
ray is computationally cheap compared to the ray tracing algorithm itself. In
\Cref{fig:svo_ray_transform}, the ray transformation stage is illustrated.
\acrshort{svo} 1 represents an \acrshort{svo} with identity transformation,
leaving the ray unchanged, while \acrshort{svo} 2 changes the ray origin and
direction as a result of its orientation.

\begin{figure}[htb!]
    \centering
    \includegraphics[width=0.8\textwidth]{figures/svo_ray_transform.pdf}
    \caption{Demonstrating the ray transformation
    process.}\label{fig:svo_ray_transform}
\end{figure}

\subsection{Mathematical formulation}
In order to implement the solution in software or hardware, a mathematical
formulation for the ray transformation is desired. Given the ray definition
shown in \Cref{eq:ray_def}, a transformation $T$ from the ray $R$ in world
co-ordinates to the ray $\hat{R}$ in local co-ordinates to the \acrshort{svo}
must be derived. The desired transformation should work as shown in
\Cref{eq:ray_transform}.
\begin{align}
    R_t(\mathbf{r}_o, \mathbf{r}_d) &= \mathbf{r}_o + t \cdot \mathbf{r}_d \ ,
    \quad t \geq 0 \label{eq:ray_def} \\
    \hat{R}_t(\hat{\mathbf{r}}_o, \hat{\mathbf{r}}_d) &= T\left[R_t(\mathbf{r}_o,
    \mathbf{r}_d)\right] \label{eq:ray_transform}
\end{align}

A graphical description of the desired transformation is shown in
\Cref{fig:ray_transform_math}. Formulating the transformation mathematically
is, at its core, a matter of deriving two matrices with which to multiply the
constituent vectors $\mathbf{r}_o$ and $\mathbf{r}_d$. These vectors represent
the ray origin and direction, respectively. By initially only allowing the
\acrshort{svo} to be rotated and translated, the linear algebra formulation is
straightforward. Given the rotation matrix $\mathbf{M}_R$ and the translation
matrix $\mathbf{M}_T$ of the \acrshort{svo}, the transformation function $T$ can
be formulated as described in the following. Since it makes no sense translating
a directional vector, it should be self-evident that the ray direction only is
influenced by the rotation of the \acrshort{svo}. The ray origin, however, is
affected by both rotation and translation.

\begin{figure}[htb!]
    \centering
    \includegraphics[width=0.8\textwidth]{figures/ray_transform_math.pdf}
    \caption{The co-ordinate system transformation of a ray in world space to
    local space.}\label{fig:ray_transform_math}
\end{figure}

The resulting mathematical definition of $T$ is shown in
\Cref{eq:ray_transform_def}. The ray direction is determined by simply
pre-multiplying it with the inverse rotation of the \acrshort{svo}.
Transforming the ray origin is a bit more involved, but may be regarded as a
two-step process. Firstly, the ray origin is translated so that the origin of
its co-ordinate system is at the origin of the octree. Secondly, the vector is
rotated around the \acrshort{svo} origin by the same inverse rotation as
employed for the ray direction. The mathematical formulation is shown in
\Cref{eq:ray_transform_def}.
\begin{equation}\label{eq:ray_transform_def}
    \begin{aligned}
        T : \; &R_t(\mathbf{r}_o, \mathbf{r}_d) \mapsto
        \hat{R}_t(\hat{\mathbf{r}}_o, \hat{\mathbf{r}}_d) \\
        &\text{such that } \begin{cases}
        \hat{\mathbf{r}}_d = \mathbf{M}^{-1}_R \mathbf{r}_d \\
        \hat{\mathbf{r}}_o = \mathbf{M}^{-1}_R \mathbf{M}^{-1}_T \mathbf{r}_o
        \end{cases}
    \end{aligned}
\end{equation}

\subsection{Extending the method to allow anisotropic scaling}
The transformation so far only accounts for \acrshort{svo} models with
transforms consisting of translation and rotation. While these two affine
transforms are the only ones strictly required to provide the functionality of
rigid-body animation, there is a third transform that one often needs in
animation. In order to fully facilitate animation, there should be a way to
animate the size of models as well, by enlarging or shrinking the models along
some or all principal axes. In other words, there should be support for
non-uniform, or anisotropic, scaling of \acrshort{svo}s. The most convenient way
of supporting scaling in the scheme presented above is to implement the support
at the traversal stage of the ray tracing process. Luckily, most \acrshort{svo}
traversal algorithms, and especially the one this thesis is based upon, already
allow the tuning of octree dimensions. By employing this directly in the
animation process, the method presented above may remain simple, and only take
the rotation and translation into account.

For instance, in the traversal algorithm presented in \Cref{sec:svo_traversal},
one simply has to input the dimensions of the octree as a set of parameters
$x_0$, $x_1$, $y_0$, $y_1$, $z_0$, and $z_1$. Do note, however, that any
calculation involving the scale of \acrshort{svo}s---such as determining the
bounding sphere---also need to be updated to take the scale of the octree into
account.

\section{Measures for improving efficiency}\label{sec:svo_optimisation}
The method described above is an improvement upon the basis method in many ways.
Yet, some measures may be taken in order to further improve the efficiency of
the method.

\subsection{Ray-sphere intersection tests}
As a result of the sheer number of intersection tests, the most computationally
heavy stage of the ray tracing process is the traversal of the \acrshort{svo}
\cite{glassner84}. Therefore it is desirable to only traverse \acrshort{svo}s
that can lead to a ray tracing hit. For instance, in a situation where one of
the \acrshort{svo}s is located some distance away from the origin of the current
ray, and the direction of the ray is pointed in the opposite direction of the
octree, the \acrshort{svo} can safely be excluded from the process, as the ray
will never hit it.

\begin{figure}[htb!]
    \centering
    \includegraphics[width=0.8\textwidth]{figures/svo_bounding_sphere.pdf}
    \caption{Using the bounding sphere of an \acrshort{svo} to avoid traversing
    octrees that will be missed. \acrshort{svo} 2 is the only octree that will
    be traversed in this case.}\label{fig:svo_bounding_sphere}
\end{figure}

The process of determining which octrees that will never be hit by a given ray
may be implemented in a multitude of ways. One method that will be looked
further into in this section is to perform an intersection test between the ray
and the bounding sphere of the \acrshort{svo}. This intersection test is very
fast compared to traversal of the entire \acrshort{svo}, and will in many cases
lead to the exclusion of octrees that will never be hit by a given ray. In
\Cref{fig:svo_bounding_sphere} the principle is illustrated. A scene of animated
\acrshort{svo}s is shown, where bounding spheres are utilised to determine which
octree models that will be missed by the ray. In this case, only \acrshort{svo}
2 will be traversed, as the bounding sphere of the other three octrees in the
scene do not intersect the ray.

The rationale behind choosing the bounding sphere to represent the bounds of
an octree model, instead of the bounding box---which at first glance may seem
more logical---is that the sphere is invariant under rotation of the
corresponding \acrshort{svo}. The only consideration one has to make when
constructing the bounding sphere of an \acrshort{svo} is the position of its
centre and its radius, both of which are readily obtainable from the
\acrshort{svo}; indeed, they are given directly by its translation and scale.
This means that the ray-sphere intersection test remains simple even though the
underlying \acrshort{svo} may have an arbitrary orientation. A drawback of using
bounding spheres instead of bounding boxes is that it will on occasion lead to
false positives. In other words, situations may arise where an octree model is
processed and traversed even though the ray does not intersect with the
\acrshort{svo}.

\subsection{Depth sorting}
The use of bounding spheres means that still further improvements to efficiency
can be made. For instance, the \acrshort{svo} models may be traced in a
front-to-back order, sorted by the distance along the ray for each intersected
bounding sphere. Once an \acrshort{svo} model is traversed and results in a ray
hit that is closer than the bounding sphere of the next \acrshort{svo} to be
traced, the ray tracing process may be stopped early, as no object can lie in
front of the current hit.

In \Cref{fig:svo_sorted}, a ray tracing scene consisting of an animated set of
\acrshort{svo}s is shown. The tracing is performed in a sorted manner, where the
order of traversal is by increasing distance to the bounding sphere centre, in
this example starting with \acrshort{svo} 1 and ending with \acrshort{svo} 4.
The figure shows a situation where \acrshort{svo} 1 is traversed without a hit,
\acrshort{svo} 2 is traversed as a false positive, \acrshort{svo} 3 results in a
trace hit, and \acrshort{svo} 4 is not traversed. The second octree can be
regarded as a false positive because the ray hits the bounding sphere, but not
the octree itself. The ray tracing process is terminated after \acrshort{svo} 3
is processed, since it results in a ray hit, and the distance along the ray of
this hit is closer than the distance to the boundary of the next octree that
would be traversed, \acrshort{svo} 4. In other words, it is mathematically
impossible that traversal of the fourth octree will yield a ray hit closer than
the hit produced by traversal of the third octree.

\begin{figure}[htb!]
    \centering
    \includegraphics[width=\textwidth]{figures/svo_sorted.pdf}
    \caption{Tracing a sorted list of \acrshort{svo}s.}\label{fig:svo_sorted}
\end{figure}

\section{Discussion}
An assessment of the solution detailed in the previous sections is now to be
made. A discussion of the efficiency of the solution will be presented, and
comparisons to other existing solutions for animation of \acrshort{svo}s will be
drawn. Subsequently, the limitations of the solution will be presented.

\subsection{Efficiency assessment}
Firstly, the objectives formulated at the beginning of the chapter will be
evaluated. The proposed solution will be compared to the basis method outlined
in the introduction to this chapter.

\subsubsection{Execution time}
Determining how well the solution meets the requirements in terms of execution
time is not straightforward. However, since the solution can be compared to the
basis method, some conclusions may be drawn.

The proposed solution does not change the internal data of the \acrshort{svo} in
any way, as the process of animation is reduced to simply updating a
transformation matrix. Hence, it should be self-evident that the execution time
will be reduced drastically when compared to the basis method. The basis method
must either store each frame of animation as its own \acrshort{svo}, or update
the octree data directly and rebuild the hierarchical structure between frames.
The former is infeasible in terms of memory bandwidth, as will be detailed
below, while the latter is computationally expensive, and presumably impossible
in real-time.

An optimisation technique utilising bounding spheres was presented. This
technique should increase the performance by limiting the number of
\acrshort{svo}s that will be traversed for a given ray in a frame. Since the
number of rays processed per frame is in the millions, the gains in execution
time yielded by such optimisations quickly add up. As will be seen in
\Cref{ch:software_demo}, such optimisations are indeed imperative to make
real-time performance possible.

\subsubsection{Memory requirements}
In terms of memory, the proposed solution does not represent a significant
increase in space. In practice, only a few more bytes are needed per
\acrshort{svo} in order to enable animation. While each octree in theory
only needs a transform matrix which specifies its translation, rotation,
and scale, it might also make sense to store the bounding sphere and scale
separately in order to speed up execution. In any case, the amount of data
that needs to be stored is minimal compared to the basis method, where each
frame of animation is, in practice, a separate octree.

As for memory bandwidth, when compared to the basis method---where a new octree
must be streamed from memory each frame---the proposed solution should perform
far better. This is because the only data that needs to be updated between
frames is the transform matrix, and in some cases the bounding sphere and scale.

\subsubsection{Hardware suitability}
For all intents and purposes, the method should be well-suited for hardware.
While the method in itself is agnostic with regard to the underlying traversal
algorithm and the \acrshort{svo} data structure, the algorithms chosen for the
software implementation showcased in \Cref{ch:software_demo} have already been
demonstrated to work in hardware by \textcite{wilhelmsen12}. As for the specific
features needed by the animation logic, matrix multiplication can easily be
accelerated by dedicated circuits. The same can be said for the ray-sphere
intersection tests. A consequence of this is that such computations may be kept
on-chip, allowing shorter critical paths, and a higher core frequency.

In the discussion of memory bandwidth above, the conclusion was drawn that the
proposed solution represents a significant improvement over the basis method,
since very little data has to be updated in memory between frames. This also
translates well into direct improvements in a hardware setting. By limiting the
memory bandwidth, a hardware implementation may avoid performing accesses to
external memory altogether, further increasing performance. In addition, since
the sparse voxel octree data itself remains unchanged when animated, one or more
levels of caching may be employed in order to reduce the latency associated with
memory accesses. Lastly, since the amount of data that describes the animation
of an octree is very small, these transformation matrices may also be stored
directly in very fast register memory close to the core, further reducing memory
access latency.

\subsection{Comparison with other methods}
The only alternative animation technique discovered in the literature is the
method presented by \textcite{bautembach11}. His solution is very different from
the one presented in this thesis. Bautembach treats every node of an
\acrshort{svo} as a singular \textit{atom} that may be transformed independently
of all other nodes in the octree. His technique can be viewed as a
\textit{bottom-up} solution to the problem, while the one presented here is a
\textit{top-down} solution.

In a similar manner to the solution presented here, Bautembach's solution does
not change the octree data itself. Each node, however, is drawn at a different
position that is computed on-the-fly. As is detailed in his paper, the solution
is not suitable for ray tracing, since the hierarchical structure of the octree
that is relied upon by most ray tracing algorithms is destroyed. The simple
deduction that child nodes are no longer necessarily contained within their
parent nodes places further emphasis on this fact. Therefore, Bautembach's
solution can not really be used for further comparison, since the premise of
the solution presented in this thesis is that it should be used in ray tracing.

\subsection{Limitations}
There are some limitations of this method that should be noted. The main
limitation is that the only type of animation supported is rigid-body animation.
This means that effects such as deformation and other, similar features are not
supported. In other words, the animation functionality provided by the solution
is mostly suited for stiff, mechanical objects and not mesh objects. The basis
method outline in the beginning of this chapter is not restricted by this, and
could in theory animate anything, also deformation.

Further, the fact is that each part of the scene that is to be animated must be
stored as a separate \acrshort{svo} with its own transform. This may not at
first seem like a serious concern, but consider a complex object with hundreds
of moving parts. This object would have to be formulated as hundreds of
\acrshort{svo}s, and it is not clear how the algorithm would respond to this in
terms of performance. A consequence is that each ray would have to be
intersection-tested with the bounding sphere of each \acrshort{svo} in the scene,
resulting in many millions of extra intersection tests that would have to be
performed each frame. As one may realise, in this case it appears that the
algorithm will not under any circumstances be able to deliver real-time
performance. Future work may include exploring measures that can be taken to
mitigate this problem. An idea to this end would be to investigate whether the
entire scene could be subdivided---for instance into a large octree---in order
to sort the objects so that only relevant \acrshort{svo}s would have to be
considered by the ray tracing algorithm.
