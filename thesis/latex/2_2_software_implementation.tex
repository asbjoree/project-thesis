\chapter{Software demonstration}\label{ch:software_demo}
A software demonstration of the scheme detailed in \Cref{ch:proposed_method} was
prepared for this thesis. The implementation was written in the C programming
language, and employs the \acrshort{api}s Nvidia \acrshort{cuda} and OpenGL; the
former for the ray tracing itself, and the latter for easing the process of
displaying the result. A description of the toolchain, as well as a discussion
of the results of testing will be presented in this chapter. The core functions
of the software can be found in \Cref{app:ray_tracer_core}.

\section{Preliminary design choice assessments}
There are several ways in which ray tracing may be implemented. Some methods
result in better visual results, and others are more computationally efficient.
Since real-time computer graphics is the topic of this thesis, it is natural to
assume that at least some aspect of computational efficiency is desired, and
that choices to this end should in many cases be preferred. On the basis of
these observations, a number of design choices were made when implementing the
scheme in software. Justifications for these are presented in the following.

\subsection{Parallelisation}
When computational efficiency is of the essence, it is generally detrimental to
limit a software implementation to sequential processing. Luckily, the concept
of ray tracing is of such a nature that it easily lends itself to
parallelisation. As described in \Cref{ch:background}, this is a consequence of
the fact that, in and of itself, each ray is independent of every other ray. For
every pixel on screen, one or more rays are spawned and traced into the scene.
The traversal of the scene by these rays ultimately produces the colour of the
individual pixels. Hence, it is logical to try to exploit the parallel nature of
ray tracing in some shape or form. While the most efficient solution would
presumably be application-specific hardware, there exists readily available
solutions that may be used to this end---the most easily available parallel
platform being the \acrshort{gpu}. More specifically, using the parallel
processing capabilities of the graphics processing hardware to run general
computational software (\acrshort{gpgpu}) may speed up the computation by many
orders of magnitude. As detailed in \Cref{ch:prev_work}, \textcite{crassin09}
makes a convincing argument for using \acrshort{gpgpu} implementations for
software ray tracing.

\subsection{Memory bandwidth}\label{sec:memory_bandwidth}
There is a desire to minimise the memory bandwidth requirements of the software,
since this can quickly turn into a bottleneck in performance---especially with
regard to the sharing of memory between the \acrshort{cpu} and \acrshort{gpu}
of a system \cite{torres12}. As shown by \textcite{yang10}, in order to fully
realise the potential of \acrshort{gpgpu}, one must consider efficient
utilisation of the \acrshort{gpu} memory hierarchy, given its dominant impact on
performance.  Keeping this in mind during the software development will arguably
lead to a more efficient design, both in terms of performance and execution time
predictability.

\subsection{Model data}\label{sec:model_data}
The scene to be traced is made up of a set of octrees employing the
\acrshort{svo} format described in detail in
\Cref{ch:elected_algo,ch:proposed_method}. Each of these \acrshort{svo}s can be
thought of as a single, self-contained model. An effort should be made to avoid
having to generate these models in a hard-coded manner on application start-up.
Models could be generated in advance by processing a polygonal model in a widely
used format, and storing it as a binary file in the correct \acrshort{svo}
format. There are three main arguments in support of this solution:

\textbf{Application performance}: Offloading the generation of the models to an
earlier stage would mean an increase in application performance---at least in
the context of start-up time. The models could then be loaded directly into
memory as a binary object in the chosen \acrshort{svo} data structure format on
application start-up.

\textbf{Model fidelity}: By not generating the models from hard-coded data
structures, the model resolution can be increased massively. Models can be
generated with arbitrary fidelity, in practice only limited by the capacity of
available memory.

\textbf{Basis of comparison}: Generating the voxel models from existing
polygonal models will provide a basis of comparison. This is advantageous since
useful comparative data results may be gathered and analysed.

\section{Rendering pipeline description}
The ray tracing core was written as a \acrshort{gpgpu} program using the Nvidia
\acrshort{cuda} \acrshort{api}. Based on the traversal algorithm detailed in
\Cref{ch:elected_algo}, the \acrshort{cuda} kernel traces the scene based on a
description of the scene itself, as well as the raw \acrshort{svo} data
initially copied to graphics memory. The image generated by the ray tracing
procedure is placed in a buffer located in graphics device memory. The data is
stored in pixel buffer object (\acrshort{pbo}) format---essentially as a block
of raw pixel data.

Running concurrently alongside the ray tracing kernel is a simple OpenGL
program, the purpose of which is to display the data generated by the ray
tracer. This OpenGL application reads the \acrshort{pbo} data directly from
graphics memory and uses the data as a texture. The texture is applied to a
simple quad spanning the entire screen of the application, and thus displays the
result of the ray tracing process to the user.

The pipeline is illustrated in \Cref{fig:pipeline}. As detailed in
\Cref{sec:memory_bandwidth}, care should be taken into minimising the required
memory bandwidth of the application, since this may lead to a bottleneck in
performance. In the \acrshort{pbo} solution described, the data is generated in
graphics memory and read directly from the same memory location.  This means
that the data stays within device memory, and does not have to go via the
\acrshort{cpu} in order to be displayed, alleviating the concerns for memory
bottlenecks.

\begin{figure}[htb!]
    \centering
    \includegraphics[width=\textwidth]{figures/pipeline.pdf}
    \caption{The pipeline used when solving the problem.}\label{fig:pipeline}
\end{figure}

\section{Implementation details}
The following sections describe details of the implementation, and provide
justifications where needed for choices made during development.

\subsection{Sparse voxel octree generation}
The sparse voxel octrees used in the software demonstration was generated from
polygonal models in a process similar to the one described in
\Cref{sec:model_data}. The toolchain for model generation is illustrated in
\Cref{fig:svo_toolchain}.

\begin{figure}[htb!]
    \centering
    \includegraphics[width=\textwidth]{figures/svo_toolchain.pdf}
    \caption{Toolchain for generating SVO models.}\label{fig:svo_toolchain}
\end{figure}

The polygonal source models were obtained from online resources such as
\textcite{stanfordmodels}, or created in the modelling software
\textcite{blender}. The models were then processed by the software
\textit{binvox}, which is a freely-distributed program written by
\textcite{binvox}. It reads a 3D model file in a widely-used format---in this
case \texttt{.ply} or \texttt{.obj}---and produces a voxel model by means of
three-dimensional rasterisation. The voxel model is stored in a custom file
format (\texttt{.binvox}), which is a raw voxel format that employs the simple
compression scheme \textit{run-length encoding} (\acrshort{rle}) to reduce the
file size \cite{binvoxformat}.

The last step of the \acrshort{svo} generation toolchain is to generate and
store the models on the format specified in \Cref{sec:svo_structure}, so that
they can quickly be loaded into memory as a binary object on application
start-up. A custom-made program to perform this conversion was developed; its
core functions are written in C++ and included in \Cref{app:svo_generation}. The
program takes a raw voxel model in the \texttt{.binvox} format, and initially
constructs a raw, inefficient octree data structure from it. Subsequently, the
octree is converted to the efficient format used in the solution, and stored to
disk in a \texttt{.svo} file.

\subsection{Tracing sparse voxel octrees}
The algorithm presented by \textcite{revelles00}, and described in detail in
\Cref{sec:svo_traversal}, is very applicable to the use case presented by this
thesis. As such, this algorithm forms the base of the ray tracer core in the
software demonstration. The implementation is generally very true to the
original algorithm as described in the paper, however, some minor modifications
must be made to the algorithm in order for it to work properly in this
particular application. Further, certain changes are made to make it even more
efficient.

\subsubsection{Co-ordinate system}
A modification that has to be made to the algorithm during its implementation is
to translate it from a left-handed co-ordinate system to the more widely-used
right-handed co-ordinate system. The justification for this conversion is that
the rest of the application, including the \acrshort{svo} model format employed,
assumes a right-handed co-ordinate system, and a discrepancy here would not be
desirable. The conversion is achieved by inverting the direction of the
$z$-axis in the algorithm description, and making the corresponding changes to
the algorithm state machine. The detailed description of the algorithm in
\Cref{sec:svo_traversal} already accounts for this modification.

\subsubsection{Stack usage reduction}
Since the algorithm is to be run on a \acrshort{gpu}, care should be taken when
programming to avoid excessive function calls, and recursion in particular. The
reason for this is that the threads that run in parallel on the \acrshort{gpu}
have very small stacks, and function calls quickly eat up available stack
capacity. This poses a problem, since the algorithm as it is defined in the
original paper is recursive, and early implementation attempts revealed that the
stack placed a hard limit on the maximum octree traversal depth of the
algorithm. However, as has already been demonstrated by \textcite{wilhelmsen12},
it is indeed possible to convert the algorithm to use an iterative approach
instead. This was done during implementation by defining a small data structure
in static, compile-time allocated memory for each thread which essentially
functions as a stack, holding all traversal state information at the current
stage. The rest of the algorithm runs in a loop and utilises this data structure
instead of the native stack.

\subsubsection{Allowing zero-valued directions}
The algorithm description in \Cref{sec:svo_traversal} introduces a set of
special cases in order to handle rays with zero-valued directions. A simpler
approach to this end was discovered during development, and entails checking
whether components of the ray direction are zero on algorithm start-up. If one
or more components are zero, they are instead assigned a very small number.
This will result in a slight distortion of the rendered image, but as long as
the value is small enough, it will not be noticeable. By solving the issue this
way, the algorithm remains simple and the need to handle special cases is
eliminated.

\subsection{Additional features and optimisations}\label{sec:hit_buffer}
There is a set of additional features and optimisations that were included in
the application. These features or optimisations are not directly related to the
algorithms and schemes which form the basis of the implementation, but are part
of the software demonstration. As such, they are presented and discussed in this
section.

\subsubsection{Hit buffer algorithm}
A simple buffering mechanism was developed in order to improve the general
performance of the application. The buffer---termed \textit{hit buffer
object} (\acrshort{hbo})---stores the ray tracing result for each pixel in the
last rendered frame. In other words, the buffer contains a data structure for
each pixel describing the last result. The idea is that if, for a given pixel,
the scene is unchanged \textit{enough} since the last frame, the traversal of
\acrshort{svo}s for this pixel may be skipped, and the value from the last frame
may be reused. The algorithm is illustrated in \Cref{fig:hit_buffer_algo}.

\begin{figure}[htb!]
    \centering
    \includegraphics[width=\textwidth]{figures/hit_buffer.pdf}
    \caption{The hit buffer algorithm.}\label{fig:hit_buffer_algo}
\end{figure}

Stored for each pixel in the \acrshort{hbo} data structure are: the colour, the
normal, the $t$-value of the hit along the ray (i.e. the depth), the index of
the \acrshort{svo} that was hit, and the nature of the hit. Most of these are
explanatory, except, perhaps, the last entry. The field describing the nature of
the hit provides information such as whether nothing was hit, or if the ray
passed through multiple bounding spheres before arriving at the hit point.

The \acrshort{hbo} is then used in conjunction with a set of state variables to
determine if the value of a pixel is unchanged. Each \acrshort{svo} in the
scene, as well as the camera, has a switch that specifies if the object has
moved since the last frame (if it is \textit{dirty}). The application can then
look up the hit buffer data for the current pixel and, if the camera is
unchanged, and the \acrshort{svo} object hit by the ray the last frame is
unchanged, simply use the last value and avoid tracing the \acrshort{svo} again.
It is also required that the ray did not pass through multiple bounding spheres
before the hit for this optimisation to take place. The reason for this
requirement is that if the ray passes through other \acrshort{svo}s, these might
have changed in the meantime, and there is no way of determining if the ray
would hit data in these \acrshort{svo}s without traversing them again.

\section{Results}
An animated model of a car rendered with the software implementation is shown in
\Cref{fig:animation}. The model consists of $2048^3$ data points and is a
digital representation of an \textit{AC Cobra 269 1963} car, taken from
\cite{accobra}. In this scene, the car body, the wheels, the doors,
and the steering wheel are each realised as separate \acrshort{svo} models with
their own transforms. The animation sequence itself is included in
\Cref{ch:car_anim}, and the figure shows three snapshots of this sequence.

\begin{figure}[htb!]
    \centering
    \begin{subfigure}[b]{0.32\textwidth}
        \includegraphics[width=\textwidth]{figures/anim1.png}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.32\textwidth}
        \includegraphics[width=\textwidth]{figures/anim2.png}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.32\textwidth}
        \includegraphics[width=\textwidth]{figures/anim3.png}
    \end{subfigure}
    \caption{Three snapshots of an animated car using the solution. The wheels
    are rolling, the doors open and close, and the steering wheel and front
    wheels turn left and right.}\label{fig:animation}
\end{figure}

\clearpage

In \Cref{fig:animation_compare}, a static, non-animated model is shown
side-by-side with an animated model for comparison. Summarised in
\Cref{tab:render_time} is the average render time and frames per second for a
selection of model types. The first model type is a static, non-animated model
of a car, as shown in \Cref{fig:car_unanimated}. The second model type is a
simple animated car where each part of the body is its own \acrshort{svo}, as
shown in \Cref{fig:car_animated}. The last model type is an animated car
consisting of the same parts as the second type, but with the bounding sphere
optimisation techniques mentioned in \Cref{sec:svo_optimisation}, and the hit
buffer mechanism described in \Cref{sec:hit_buffer} enabled. The bounding
spheres used in optimisation are highlighted in \Cref{fig:car_bounding_sphere}.

\vfill

\begin{figure}[htb!]
    \centering
    \begin{subfigure}[b]{0.42\textwidth}
        \includegraphics[width=\textwidth]{figures/car_unanimated.png}
        \caption{Non-animated car}\label{fig:car_unanimated}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.42\textwidth}
        \includegraphics[width=\textwidth]{figures/car_animated.png}
        \caption{Animated car}\label{fig:car_animated}
    \end{subfigure}
    \caption{A static, non-animated car compared to an animated car. Both models
    have a resolution of $2048^3$ voxels.}\label{fig:animation_compare}
\end{figure}

\vfill

\begin{table}[htb!]
    \renewcommand{\arraystretch}{1.3}
    \centering
    \caption{Average frame rendering time for different model types. The render
    time is averaged over 10 seconds. Frames per second are calculated as the
    inverse of the average render time.}
    \begin{tabular}{lrr}
        \toprule
        Model type & Render time & Frames per second \\
        \midrule
        Non-animated & 51.82 ms & 19.30 FPS \\
        Animated & 71.38 ms & 14.01 FPS \\
        Animated (with optimisations) & 52.55 ms & 19.03 FPS \\
        \bottomrule
    \end{tabular}\label{tab:render_time}
\end{table}

\vfill

\clearpage

\begin{figure}[htb!]
    \centering
    \includegraphics[width=0.9\textwidth]{figures/car_bounding_sphere.png}
    \caption{Car model with the bounding spheres used for optimisation purposes
    highlighted.}\label{fig:car_bounding_sphere}
\end{figure}

\section{Discussion}
A discussion of the results gathered from the software implementation is
presented in the following. The majority of the discussion regarding the method
itself has already been presented in \Cref{ch:proposed_method}.

\subsection{Performance}
As is apparent from the numerical values shown in \Cref{tab:render_time}, the
difference in raw performance between the static model and the animated model
with optimisations enabled is almost negligible. This result testifies to the
claim that the solution presented in this thesis is suitable for animation of
sparse voxel octree data. And since the animation technique presented in this
thesis does not introduce a noticeable overhead, further improvements in
performance may be feasible with even more optimisation in the traversal
algorithm itself. As indicated by the performance counters, most execution time
is spent in this section of the software, not as a part of the animation process
itself.

It can be argued whether the performance shown should be classified as animation
in real-time. This will first require a definition of the term
\textit{real-time}. The concept is here taken to have a somewhat different
meaning than it does in the field of embedded systems. While the term has a
rigorous definition there, it is here used to judge the performance of an
animation technique. Therefore it is taken to mean any performance that will be
perceived as motion by the human eye.  The measure of frames per second in
relation to human motion perception, and at what stage a series of frames is
perceived as animation, is a topic outside the scope of this thesis. However,
according to \textcite[24]{read00}, 16 frames per second is enough to adequately
provide visual continuity so that a set of frames may be perceived as motion.
Nonetheless, they go on to explain that with anything below 24 frames per
second, a flicker may be perceived, for instance in changes in brightness.

Based on these numbers, the performance shown in the
results may be classified as real-time. It should also be noted that the
\acrshort{gpu} employed when gathering the data, the \textcite{gtx680}, was
released in 2012. The graphics card is over 6 years old at the time of writing,
so performance of the software demonstration is likely to have improved
substantially if run on newer hardware with better \acrshort{cuda}
specifications.

\subsection{Visual fidelity}
The models rendered with the software implementation and shown in the figures
earlier in this chapter have a resolution of 2048 voxels in each dimension. In
other words, if the models were scaled so that each side had a length of 1
metres, the smallest detail that could be discerned would be around 0.5 mm. For
most models in a given scene, this resolution ought to be enough in terms of
detail.

However, due to the way the normals are currently calculated, some erroneous
visual effects appear. These artefacts can for instance be spotted in
\Cref{fig:animation_compare}, where one may notice certain aliasing effects on
the model. The artefacts stem from the fact that the software implementation
currently does not use normal vectors stored in the model itself, but simply
calculates a surface normal vector at run-time based on which face of a given
voxel that is hit by the ray.  If the \acrshort{svo} model format were extended
to include surface normals on a per-voxel basis, the visual fidelity would
increase dramatically, as the aliasing effects would all but disappear.
Including continuous normals would also allow for more complex lighting
calculations such as refraction, reflection, and specular effects.

A simple demonstration was written in support of this argument, and the results
are shown in \Cref{fig:continuous_normals}. In the illustration, a sphere is
rendered with the two different normal configurations. The sphere only has a
resolution of 64 voxels in each dimension, so one would expect the visual
fidelity to be even better with higher resolution models, such as the car
rendered earlier in this chapter.  The sphere in \Cref{fig:cont_normals} also
demonstrates one of the more sophisticated lighting models that may be
implemented. In this case, ambient light, diffuse light, and specular highlights
are modelled, as described by the \textit{Phong reflection model}, formulated by
\textcite{phong75}.

\begin{figure}[htb!]
    \centering
    \begin{subfigure}[b]{0.42\textwidth}
        \includegraphics[width=\textwidth]{figures/face_normals.png}
        \caption{Face-based normals}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.42\textwidth}
        \includegraphics[width=\textwidth]{figures/continuous_normals.png}
        \caption{Continuous normals}\label{fig:cont_normals}
    \end{subfigure}
    \caption{Normals calculated based on hit face compared to continuous normals
    stored in the model. Both models have a resolution of
    $64^3$.}\label{fig:continuous_normals}
\end{figure}

This normal vector feature is not currently implemented in the software
demonstration, and could thus be a part of future work on the results of this
thesis. Indeed, the specification of the \acrshort{svo} data structure format by
\textcite{laine11} does present a way for normals to be included as an extension
to the \acrshort{svo} format. This might even increase the general performance
of the algorithm since the normal vectors could be fetched directly from the
model instead of being calculated explicitly for each ray.

\subsection{Limitations of the optimisation techniques}
There is a limit to the region of applicability for most optimisation
techniques. In this case, the hit buffer optimisation introduced in
\Cref{sec:hit_buffer} is limited in that becomes ineffective once the camera
moves. This is a consequence of the fact that once the camera moves in some
fashion, most rays of the algorithm have also necessarily been changed since the
last frame. As such, the previous results stored in the hit buffer are no longer
applicable, and the entire scene must be retraced. The bounding sphere
optimisation introduced in \Cref{sec:svo_optimisation} is still effective,
however, as it is an optimisation in world space, not in screen space. In other
words, it is a part of the ray tracing algorithm itself, and not dependent on
the camera. Thus, it should provide a performance gain regardless of camera
movement.

Further investigation into optimisation techniques such as the bounding sphere
optimisation, as well as improving the \acrshort{hbo} optimisation technique are
topics that could be part of future research.

\subsection{Suitability for hardware implementation}
The method's suitability for hardware implementation was discussed in detail in
\Cref{ch:proposed_method}, where it was argued that the general idea is
well-suited for hardware acceleration. Since borderline real-time performance
has already been demonstrated when running the method in software, it is very
exciting to think of the possible increase in performance that may come about if
one were to implement the method in application-specific hardware. It is not
far-fetched to draw the conclusion that ray tracing may have a central role in
the future of real-time computer graphics. Implementing the method in hardware
is certainly something that could be explored in future work.


\subsection{Additional data}
Please note that the results presented in this chapter are gathered from a
single animated model, and may not represent the general case. It would be
advantageous to run the implementation on additional models such that more data
can be gathered. This could be used to further investigate the claim that the
animation does not tax the performance noticeably, and help gain understanding
of how the algorithm responds to the rendering of different data sets.

