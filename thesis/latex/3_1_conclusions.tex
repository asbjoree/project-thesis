\chapter{Conclusions}\label{ch:conclusions}
In this thesis a method for animation of sparse voxel octrees was presented. The
method is designed for real-time ray tracing, and is therefore optimised for
this application. It employs the works of \textcite{revelles00}, and
\textcite{laine11} as foundation, where the former paper provided the octree
traversal algorithm, and the latter contains the formulation upon which the
\acrshort{svo} format is based. No previous attempts of animating sparse voxel
octrees for use in ray tracing were found in the literature. The method by
\textcite{bautembach11} does specify a technique for animation of sparse voxel
octrees, but---as he states explicitly himself---his work is not suited for ray
tracing applications.

The proposed solution was presented in \Cref{ch:proposed_method}, where it was
subsequently detailed and discussed. A comparative foundation was established,
termed the \textit{basis} method, which naïvely reconstructs the octree data
structure for each frame. In the discussion, it was argued that the performance
of the proposed solution should be far superior to that of the basis method. In
addition, its memory requirements and suitability for hardware implementation
were evaluated. The conclusions were drawn that an animated \acrshort{svo}
requires a negligible increase in memory usage and bandwidth compared to a
non-animated, static model, and that the method itself is indeed suitable for
hardware implementation. Moreover, some general optimisation techniques relevant
to the solution were detailed, for instance the usage of bounding spheres to
enable a form of lazy traversal.

A software implementation employing the Nvidia \acrshort{cuda} \acrshort{api}
was prepared for the sake of gathering empirical data in the evaluation of the
proposed solution. In \Cref{ch:software_demo}, this implementation was studied
and evaluated. The chapter culminated in the demonstration of a car model
animated with borderline real-time performance. In the subsequent discussion,
its efficiency and visual fidelity were evaluated. It was argued that one could
expect further improvements in performance if the solution were to be run on
application-specific hardware. An optimisation technique termed \textit{hit
buffer object} (\acrshort{hbo}) was also presented in this chapter.

In conclusion, the method presented in this thesis does indeed appear to satisfy
most, if not all, the requirements stated in the problem text. Whether it is
flexible enough to meet the needs of the computer graphics industry remains to
be seen. Its applicability will also depend on what the future holds for ray
tracing itself. Due to the dominance of the rasterisation technique in the field
of image synthesis, it is uncertain whether ray tracing will ever be widely
used.  However, certain new and interesting developments in the field---such as
Nvidia's newest line of \acrshort{gpu}s---do indicate a brighter future for ray
tracing, and it should be very exciting to follow what might unfold in the
industry over the next few years.

\section{Limitations}
The limitations of the animation technique were detailed in
\Cref{ch:proposed_method}, but the main points will be reiterated here---for
completeness' sake. The algorithm only supports rigid-body animation, which
means that it is only relevant for certain kinds of animation sequences. For
instance, it cannot easily be used to model deformation and mesh animation such
as a flexing muscle or a waving flag.  Another limitation of the method is that
each animated part of the scene has to be formulated as its own \acrshort{svo}.
This can lead to a situation where a given scene may consist of a huge number of
objects that have to be considered by the traversal algorithm.

Limitations of the software implementation were also detailed. Currently, the
implementation does not support surface normals to a satisfactory degree. The
normals are at this time determined in the traversal algorithm by looking at
which face of a voxel a given ray hits. As a consequence, models appear
``blocky'', and this may in turn give rise to certain aliasing artefacts.
Certain parts of the optimisations implemented are also limited to some degree.
For instance, the \acrshort{hbo} optimisation technique becomes ineffective once
the camera moves.

\section{Future work}
The solution presented in this thesis opens up for quite a few possibilities in
terms of continued research. The main area that should be explored is further
optimisation. In particular, an attempt to implement the solution in hardware
would be very exciting, as it would presumably lead to even better performance.
This section of future work is a part of what the author expects he will focus
on when writing his master's thesis.

Other kinds of optimisations that may be explored is to extend the bounding
sphere optimisation detailed in \Cref{ch:proposed_method} even further. One may,
for instance, investigate the possibility of sorting the \acrshort{svo}s that
make up a scene into another data structure, such as another octree, to increase
support for a larger number of models.

It would also be interesting to see how high-resolution models, such as the car
rendered in \Cref{ch:software_demo}, would look with proper surface normals.
These normals could be used in an implementation of a superior lighting model,
such as the phong illumination model \cite{phong75}. As such, extending the
\acrshort{svo} model format to include normals is certainly part of future work.

Further evaluation should be performed, for instance by running the
implementation on several different scenes. Since the current results only stem
from one animated model, there is simply not enough empirical data to fully
support the claim that the technique does not significantly slow down the
rendering process.
