#include <cuda.h>

#include "settings.h"
#include "math_helpers.h"
#include "scene.h"
#include "animation.h"


__global__ void animation_kernel(scene_t scene, double time)
{
    for (uint8_t i = 0; i < scene.svo_count; i++) {
        scene.svo[i].dirty = false;
    }

    time *= 0.7; // Slow down a bit

    // Wheels
    scene.svo[1].position = make_float3(0.28, -0.07, -0.17);
    scene.svo[1].rotation = make_float3(0.0, M_PI, -time);
    scene.svo[1].scale = 0.17;
    scene.svo[1].dirty = true;

    scene.svo[2].position = make_float3(-0.31, -0.07, 0.17);
    scene.svo[2].rotation = make_float3(0.0, sin(time) * 0.5, time);
    scene.svo[2].scale = 0.17;
    scene.svo[2].dirty = true;

    scene.svo[3].position = make_float3(-0.31, -0.07, -0.17);
    scene.svo[3].rotation = make_float3(0.0, M_PI + sin(time) * 0.5, -time);
    scene.svo[3].scale = 0.17;
    scene.svo[3].dirty = true;

    scene.svo[4].position = make_float3(0.28, -0.07, 0.17);
    scene.svo[4].rotation = make_float3(0.0, 0.0, time);
    scene.svo[4].scale = 0.17;
    scene.svo[4].dirty = true;

    // Car doors
    float t = (sin(time / 2) + 1.0) / 2 * M_PI / 2;
    float st = sin(t);
    float ct = cos(t);
    scene.svo[5].position = make_float3(0.0205, -0.022, 0.167) + make_float3(0.06 * cos(t), 0.0, 0.11 * sin(t));
    scene.svo[5].rotation = make_float3(0.0, -t, 0.0);
    scene.svo[5].scale = 0.195;
    scene.svo[5].dirty = true;

    scene.svo[6].position = make_float3(0.0205, -0.022, -0.167) + make_float3(0.06 * cos(t), 0.0, -0.11 * sin(t));
    scene.svo[6].rotation = make_float3(0.0, t, 0.0);
    scene.svo[6].scale = 0.195;
    scene.svo[6].dirty = true;

    // Steering wheel
    scene.svo[7].position = make_float3(0.0, 0.0, -0.1);
    scene.svo[7].rotation = make_float3(-0.3, M_PI / 2, sin(time) * 3.0);
    scene.svo[7].scale = 0.08;
    scene.svo[7].dirty = true;
}
