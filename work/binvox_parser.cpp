#include <stdio.h>
#include <cstring>
#include <cstdlib>
#include <stdint.h>

#include "binvox_parser.h"

void binvox_convert_to_raw_data(uint8_t* rle_data, uint8_t* raw_data, uint64_t length)
{
    for (uint64_t cursor = 0; cursor < length;) {
        uint8_t value = rle_data[0];
        uint8_t count = rle_data[1];

        memset(raw_data + cursor, value, count);
        cursor += count;
        rle_data += 2;
    }
}

int binvox_validate(
        uint8_t* data,
        uint32_t length,
        uint32_t* version,
        uint32_t* dimx,
        uint32_t* dimy,
        uint32_t* dimz,
        uint32_t* data_offset
        )
{
    if (memcmp(data, "#binvox", 7)) {
        printf("Error! The file is not in valid binvox format.\n");
        return 1;
    }

    const char* format = "#binvox %d\ndim %d %d %d";
    sscanf((char*)data, format, version, dimx, dimy, dimz);

    printf("Version: %d\n", *version);
    printf("Dimensions: %d %d %d\n", *dimx, *dimy, *dimz);

    uint32_t d_offset = 0;
    for(; d_offset < length; d_offset++) {
        if (memcmp(data + d_offset, "\ndata\n", 6) == 0) {
            d_offset += 6;
            break;
        }
    }

    *data_offset = d_offset;

    printf("Data offset: %d\n", *data_offset);
    return 0;
}

int binvox_parse(uint32_t length, uint8_t* buffer, uint8_t** raw_data, uint32_t* dim)
{
    uint32_t version, dimx, dimy, dimz, data_offset;

    binvox_validate(buffer, length, &version, &dimx, &dimy, &dimz, &data_offset);

    if (dimx != dimy || dimx != dimz || (dimx & (dimx - 1)) != 0) {
        printf("Error! Invalid dimensions. All three dimensions should be equal and a power of two.\n");
        free(buffer);
        return 1;
    }

    *dim = dimx;
    uint64_t total_size = *dim;
    total_size = total_size * total_size * total_size;

    uint8_t* rle_data = buffer + data_offset;
    *raw_data = (uint8_t*)malloc(total_size * sizeof(char));
    binvox_convert_to_raw_data(rle_data, *raw_data, total_size);

    return 0;
}
