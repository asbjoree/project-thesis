#include <stdio.h>
#include <stdint.h>
#include <cstdlib>
#include <cstring>

#include "state_info.h"
#include "binvox_parser.h"
#include "obj_parser.h"
#include "svo_generator.h"
#include "utilities.h"

int main(int argc, char* argv[])
{
    if (argc < 4 || !((!strcmp(argv[1], "-b") && argc == 4) || (!strcmp(argv[1], "-o") && argc == 5))) {
        printf("Usage: generate_svo -b <binvox file> <svo file out>\n");
        printf("or     generate_svo -o <obj file> <svo file out> <dim>\n");
        return 1;
    }

    state_info_t sInfo;
    sInfo.verbose = true;

    char* mode = argv[1];
    char* infile = argv[2];
    char* outfile = argv[3];

    uint32_t length;
    uint8_t* buffer;
    if (read_file(infile, &length, &buffer, &sInfo)) {
        return 1;
    }

    uint32_t dim;
    uint8_t* raw_data;

    if (!strcmp(mode, "-b")) {
        printf("Parsing file in binvox mode.\n");
        if (binvox_parse(length, buffer, &raw_data, &dim)) {
            return 1;
        }
    } else if (!strcmp(mode, "-o")) {
        sscanf(argv[4], "%d", &dim);
        printf("Parsing file in obj mode. Dimensions: %d x %d x %d\n", dim, dim, dim);
        if (obj_parse(length, buffer, &raw_data, dim)) {
            return 1;
        }
    }

    free(buffer);

    uint32_t svo_size;
    uint32_t* svo_entries;

    if (generate_svo(raw_data, dim, &svo_size, &svo_entries)) {
        return 1;
    }

    free(raw_data);


    if (write_file(outfile, svo_size * (sizeof(uint32_t) / sizeof(uint8_t)), (uint8_t*)svo_entries, &sInfo)) {
        free(svo_entries);
        return 1;
    }

    free(svo_entries);

    return 0;
}
