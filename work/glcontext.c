#include <GL/glew.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include "settings.h"
#include "glcontext.h"

GLuint vao, vbo, ibo;
GLuint vertexShader, fragmentShader, shaderProgram;
GLuint pbo_buffer, pbo_texture;

GLuint compileShader(const char* source, GLuint type)
{
    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &source, NULL);
    glCompileShader(shader);

    GLint status;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

    if (status != GL_TRUE) {
        char buffer[512];
        glGetShaderInfoLog(shader, 512, NULL, buffer);
        printf("Shader compile error: %s\n", buffer);
        return 0;
    }

    return shader;
}

GLuint setupVertexShader()
{
    const char* vertexSource = R"glsl(
    #version 330 core

    in vec2 position;
    in vec2 texcoord;

    out vec2 texcoord_frag;

    void main()
    {
        texcoord_frag = texcoord;
        gl_Position = vec4(position, 0.0, 1.0);
    }
    )glsl";

    return compileShader(vertexSource, GL_VERTEX_SHADER);
}

GLuint setupFragmentShader()
{
    const char* fragmentSource = R"glsl(
    #version 330 core

    in vec2 texcoord_frag;

    out vec4 outColor;
    uniform sampler2D tex;

    void main()
    {
        outColor = texture(tex, texcoord_frag);
    }
    )glsl";

    return compileShader(fragmentSource, GL_FRAGMENT_SHADER);
}

void createTexture(void)
{
    // Set up PBO output buffer
    glGenBuffers(1, &pbo_buffer);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pbo_buffer);
    glBufferData(GL_PIXEL_UNPACK_BUFFER, W_WIDTH * W_HEIGHT * sizeof(float) * 4, NULL, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);

    // Set up the texture object
    glGenTextures(1, &pbo_texture);
    glBindTexture(GL_TEXTURE_2D, pbo_texture);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
}

void setTextureData()
{
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pbo_buffer);
    glBindTexture(GL_TEXTURE_2D, pbo_texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, W_WIDTH, W_HEIGHT, 0, GL_RGBA, GL_FLOAT, NULL);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
}

uint32_t setupGL(void)
{
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    // Set up vertex buffer
    float vertices[] = {
    //  Position      Texcoords
        -1.0f,  1.0f, 0.0f, 0.0f, // Top-left
         1.0f,  1.0f, 1.0f, 0.0f, // Top-right
         1.0f, -1.0f, 1.0f, 1.0f, // Bottom-right
        -1.0f, -1.0f, 0.0f, 1.0f  // Bottom-left
    };

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // Set up index buffer
    GLuint indices[] = {
        0, 1, 2,
        2, 3, 0
    };

    glGenBuffers(1, &ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // Set up shaders
    GLuint vertexShader = setupVertexShader();
    GLuint fragmentShader = setupFragmentShader();

    if (!vertexShader || !fragmentShader) {
        return 0;
    }

    // Shader program
    shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glBindFragDataLocation(shaderProgram, 0, "outColor");
    glLinkProgram(shaderProgram);
    glUseProgram(shaderProgram);

    // Layout of vertex data
    GLint posAttrib = glGetAttribLocation(shaderProgram, "position");
    GLint texAttrib = glGetAttribLocation(shaderProgram, "texcoord");
    glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 0);
    glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));
    glEnableVertexAttribArray(posAttrib);
    glEnableVertexAttribArray(texAttrib);

    // Textures
    createTexture();
    glUniform1i(glGetUniformLocation(shaderProgram, "tex"), 0);

    return pbo_buffer;
}

void renderGL()
{
    setTextureData();

    glClearColor(1.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

void destroyGL(void)
{
    glDeleteTextures(1, &pbo_texture);
    glDeleteBuffers(1, &pbo_buffer);

    glDeleteProgram(shaderProgram);
    glDeleteShader(fragmentShader);
    glDeleteShader(vertexShader);

    glDeleteBuffers(1, &ibo);
    glDeleteBuffers(1, &vbo);

    glDeleteVertexArrays(1, &vao);
}

