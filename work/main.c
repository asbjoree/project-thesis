#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <pthread.h>

#include "settings.h"
#include "state_info.h"
#include "glcontext.h"
#include "renderer.h"
#include "screenshot.h"

GLFWwindow* window;


int createWindow(state_info_t* sInfo)
{
    if (!glfwInit()) {
        printf("Error initializing GLFW!\n");
        return 1;
    }
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    window = glfwCreateWindow(W_WIDTH, W_HEIGHT, "ray_tracer", NULL, NULL); // Windowed

    if (window == NULL) {
        printf("Error creating GLFW window!\n");
        return 1;
    }

    glfwMakeContextCurrent(window);
    glfwSwapInterval(0);

    if (sInfo->movement) {
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        glewExperimental = GL_TRUE;
    }

    if (glewInit()) {
        printf("Error initializing GLEW!\n");
        return 1;
    }

    glfwMakeContextCurrent(NULL);

    return 0;
}

double last_xpos = 0, last_ypos = 0;

void initGLFWEvents()
{
    glfwGetCursorPos(window, &last_xpos, &last_ypos);
}

void handleGLFWEvents(float* dx, float* dy, float* dz, float* db, float* de)
{
    glfwPollEvents();

    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, GL_TRUE);
    }

    if (glfwGetKey(window, MOVEMENT_FORWARD) == GLFW_PRESS) {
        *dz = -MOVEMENT_SPEED;
    } else if (glfwGetKey(window, MOVEMENT_BACKWARD) == GLFW_PRESS) {
        *dz = MOVEMENT_SPEED;
    } else {
        *dz = 0.0;
    }

    if (glfwGetKey(window, MOVEMENT_LEFT) == GLFW_PRESS) {
        *dx = -MOVEMENT_SPEED;
    } else if (glfwGetKey(window, MOVEMENT_RIGHT) == GLFW_PRESS) {
        *dx = MOVEMENT_SPEED;
    } else {
        *dx = 0.0;
    }

    if (glfwGetKey(window, MOVEMENT_UP) == GLFW_PRESS) {
        *dy = MOVEMENT_SPEED;
    } else if (glfwGetKey(window, MOVEMENT_DOWN) == GLFW_PRESS) {
        *dy = -MOVEMENT_SPEED;
    } else {
        *dy = 0.0;
    }

    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);

    *db = -(xpos - last_xpos) * MOUSE_SENSITIVITY;
    *de = -(ypos - last_ypos) * MOUSE_SENSITIVITY;

    last_xpos = xpos;
    last_ypos = ypos;
}

void runTime(state_info_t* sInfo)
{
    static double lastTime = 0.0;
    static double bmBegin = 0.0;
    static int splits = 0;
    static int lastFrameCnt = 0;
    const char* fmt_str = "Time: %6.2f, FPS: %6.2f, RenderTime: %6.2f, Frames: %3d\n";

    sInfo->time = glfwGetTime();

    if (lastTime == 0.0) {
        lastTime = sInfo->time;
        bmBegin = sInfo->time;
        lastFrameCnt = sInfo->frameCnt;
        if (sInfo->fps) {
            printf("========  Benchmark start  ========\n");
            printf(fmt_str, 0.0, 0.0, 0.0, 0);
        }
    }

    double frameTime = sInfo->time - lastTime;
    int frames = sInfo->frameCnt - lastFrameCnt;

    if (frameTime >= 5.0) {
        splits++;
        double fps = (double)frames / frameTime;
        double renderTime = frameTime / (double)frames * 1000.0;
        if (sInfo->fps) {
            printf(fmt_str, sInfo->time, fps, renderTime, frames);
        }

        lastTime = sInfo->time;
        lastFrameCnt = sInfo->frameCnt;
    }

    if (splits >= 12) {
        double bmTime = sInfo->time - bmBegin;
        double fps = (double)sInfo->frameCnt / bmTime;
        double renderTime = bmTime / (double)sInfo->frameCnt * 1000.0;
        if (sInfo->fps) {
            printf("========   Benchmark end   ========\n");
            printf("Animation: %d | Optimisation: %d\n", sInfo->animation, sInfo->optimise);
            printf("Total time:   %7.2f s\n", bmTime);
            printf("Total frames: %4d\n", sInfo->frameCnt);
            printf("Average frame rate:  %6.2f Hz\n", fps);
            printf("Average render time: %6.2f ms\n\n", renderTime);
            glfwSetWindowShouldClose(window, GL_TRUE);
        }
    }


    usleep(15000);
}

void* renderLoop(void* arg)
{
    state_info_t* sInfo = (state_info_t*)arg;

    glfwMakeContextCurrent(window);

    while(!glfwWindowShouldClose(window)) {
        sInfo->frameCnt++;

        runCuda(sInfo);
        renderGL();
        glfwSwapBuffers(window);

        if (sInfo->screenshots) {
            saveScreenshot(sInfo);
        }
    }

    destroyGL();
    destroyCuda();
}

void usage()
{
    printf("usage: ray_tracer [-v] [-f] [-a] [-o] [-c] [-m2 | -m4] [-s]\n");
}

int arguments( int argc, char** argv, state_info_t* sInfo)
{
    for (int i = 1; i < argc; ++i) {
        if (!strcmp(argv[i], "-v")) {
            sInfo->verbose = true;
        } else if (!strcmp(argv[i], "-f")) {
            sInfo->fps = true;
        } else if (!strcmp(argv[i], "-a")) {
            sInfo->animation = true;
        } else if (!strcmp(argv[i], "-o")) {
            sInfo->optimise = true;
        } else if (!strcmp(argv[i], "-s")) {
            sInfo->screenshots = true;
        } else if (!strcmp(argv[i], "-c")) {
            sInfo->movement = true;
        } else if (!strcmp(argv[i], "-m4")) {
            sInfo->msaa = 4;
        } else if (!strcmp(argv[i], "-m2")) {
            sInfo->msaa = 2;
        } else {
            printf("Unknown argument \"%s\".\n", argv[i]);
            usage();
            return 1;
        }
    }

    if (sInfo->verbose) {
        printf("Running with the following attributes:\n");

        if (sInfo->fps) {
            printf("\tShow FPS ON\n");
        } else {
            printf("\tShow FPS OFF\n");
        }

        if (sInfo->animation) {
            printf("\tAnimation ON\n");
        } else {
            printf("\tAnimation OFF\n");
        }

        if (sInfo->optimise) {
            printf("\tOptimisations ON\n");
        } else {
            printf("\tOptimisations OFF\n");
        }

        if (sInfo->screenshots) {
            printf("\tScreenshots ON\n");
        } else {
            printf("\tScreenshots OFF\n");
        }

        if (sInfo->movement) {
            printf("\tMovement ON\n");
        } else {
            printf("\tMovement OFF\n");
        }

        if (sInfo->msaa == 1) {
            printf("\tMSAA OFF\n");
        } else {
            printf("\tMSAA %dx\n", sInfo->msaa);
        }
    }

    return 0;
}

int main(int argc, char** argv)
{
    state_info_t sInfo;
    sInfo.frameCnt = 0;
    sInfo.verbose = false;
    sInfo.fps = false;
    sInfo.animation = false;
    sInfo.optimise = false;
    sInfo.screenshots = false;
    sInfo.movement = false;
    sInfo.msaa = 1;

    if (arguments(argc, argv, &sInfo)) {
        return 1;
    }


    if (createWindow(&sInfo)) {
        return 1;
    }

    initGLFWEvents();

    glfwMakeContextCurrent(window);

    uint32_t pbo_buffer = setupGL();
    setupCuda(pbo_buffer, &sInfo);

    glfwMakeContextCurrent(NULL);


    pthread_t fps_thread;
    pthread_create(&fps_thread, NULL, renderLoop, &sInfo);

    float dx, dy, dz, db, de;

    while(!glfwWindowShouldClose(window)) {
        runTime(&sInfo);
        handleGLFWEvents(&dx, &dy, &dz, &db, &de);
        if (sInfo.movement) {
            updateCamera(dx, dy, dz, db, de);
        }
    }

    pthread_join(fps_thread, NULL);


    glfwTerminate();
    return 0;
}

