#pragma once

typedef struct {
    float m11, m12, m13, m14;
    float m21, m22, m23, m24;
    float m31, m32, m33, m34;
    float m41, m42, m43, m44;
} mat4;

typedef struct {
    float3 orig, dir;
} ray_t;

typedef struct {
    float3 center;
    float radius;
} sphere_t;

typedef struct {
    float3 center;
    float3 normal;
} plane_t;

typedef struct {
    float3 position;
    float3 rotation;
    float far;
    float near;
    float fov;
    float aspect;
    bool dirty;
} camera_t;


//==============================================================================
//  Float3 functions
//==============================================================================
inline __host__ __device__ float3 operator +(float3 a, float3 b)
{
    return make_float3(a.x + b.x, a.y + b.y, a.z + b.z);
}

inline __host__ __device__ float3 operator -(float3 a, float3 b)
{
    return make_float3(a.x - b.x, a.y - b.y, a.z - b.z);
}

inline __host__ __device__ float3 operator -(float3 a)
{
    return make_float3(-a.x, -a.y, -a.z);
}

inline __host__ __device__ float3 operator *(float3 v, float a)
{
    return make_float3(v.x * a, v.y * a, v.z * a);
}

inline __host__ __device__ float3 operator *(float a, float3 v)
{
    return v * a;
}

inline __host__ __device__ float3 operator /(float3 v, float a)
{
    return make_float3(v.x / a, v.y / a, v.z / a);
}

inline __host__ __device__ float dot(float3 a, float3 b)
{
    return a.x * b.x + a.y * b.y + a.z * b.z;
}

inline __host__ __device__ float3 cross(float3 a, float3 b)
{
    return make_float3(a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x);
}

inline __host__ __device__ float len2(float3 a)
{
    return dot(a, a);
}

inline __host__ __device__ float len(float3 a)
{
    return sqrt(len2(a));
}

inline __host__ __device__ float3 normalize(float3 a)
{
    return a / len(a);
}


//==============================================================================
//  Float4 functions
//==============================================================================
inline __host__ __device__ float4 operator +(float4 a, float4 b)
{
    return make_float4(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);
}

inline __host__ __device__ float4 operator -(float4 a, float4 b)
{
    return make_float4(a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w);
}

inline __host__ __device__ float4 operator -(float4 a)
{
    return make_float4(-a.x, -a.y, -a.z, -a.w);
}

inline __host__ __device__ float4 operator *(float4 v, float a)
{
    return make_float4(v.x * a, v.y * a, v.z * a, v.w * a);
}

inline __host__ __device__ float4 operator *(float a, float4 v)
{
    return v * a;
}

inline __host__ __device__ float4 operator /(float4 v, float a)
{
    return make_float4(v.x / a, v.y / a, v.z / a, v.w / a);
}

inline __host__ __device__ float dot(float4 a, float4 b)
{
    return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
}

inline __host__ __device__ float len2(float4 a)
{
    return dot(a, a);
}

inline __host__ __device__ float len(float4 a)
{
    return sqrt(len2(a));
}

inline __host__ __device__ float4 normalize(float4 a)
{
    return a / len(a);
}


//==============================================================================
//  Mat4 functions
//==============================================================================
inline __host__ __device__ mat4 make_mat4(
    float m11, float m12, float m13, float m14,
    float m21, float m22, float m23, float m24,
    float m31, float m32, float m33, float m34,
    float m41, float m42, float m43, float m44
)
{
    return (mat4) {
        m11, m12, m13, m14,
        m21, m22, m23, m24,
        m31, m32, m33, m34,
        m41, m42, m43, m44,
    };
}

inline __host__ __device__ mat4 zero_mat4()
{
    return make_mat4(
        0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0
    );
}

inline __host__ __device__ mat4 identity_mat4()
{
    return make_mat4(
        1.0, 0.0, 0.0, 0.0,
        0.0, 1.0, 0.0, 0.0,
        0.0, 0.0, 1.0, 0.0,
        0.0, 0.0, 0.0, 1.0
    );
}

inline __host__ __device__ mat4 make_mat4(float4 a, float4 b, float4 c, float4 d)
{
    return make_mat4(
        a.x, a.y, a.z, a.w,
        b.x, b.y, b.z, b.w,
        c.x, c.y, c.z, c.w,
        d.x, d.y, d.z, d.w
    );
}

inline __host__ __device__ mat4 make_rotation_x(float theta)
{
    mat4 result = identity_mat4();

    result.m22 = cos(theta);
    result.m23 = -sin(theta);
    result.m32 = sin(theta);
    result.m33 = cos(theta);

    return result;
}

inline __host__ __device__ mat4 make_rotation_y(float theta)
{
    mat4 result = identity_mat4();

    result.m11 = cos(theta);
    result.m13 = sin(theta);
    result.m31 = -sin(theta);
    result.m33 = cos(theta);

    return result;
}

inline __host__ __device__ mat4 make_rotation_z(float theta)
{
    mat4 result = identity_mat4();

    result.m11 = cos(theta);
    result.m12 = -sin(theta);
    result.m21 = sin(theta);
    result.m22 = cos(theta);

    return result;
}

inline __host__ __device__ mat4 make_translation(float3 translation)
{
    mat4 result = identity_mat4();

    result.m14 = -translation.x;
    result.m24 = -translation.y;
    result.m34 = -translation.z;

    return result;
}

inline __host__ __device__ mat4 transpose(mat4 m)
{
    mat4 result = identity_mat4();

    result.m11 = m.m11;
    result.m12 = m.m21;
    result.m13 = m.m31;
    result.m14 = m.m41;
    result.m21 = m.m12;
    result.m22 = m.m22;
    result.m23 = m.m32;
    result.m24 = m.m42;
    result.m31 = m.m13;
    result.m32 = m.m23;
    result.m33 = m.m33;
    result.m34 = m.m43;
    result.m41 = m.m14;
    result.m42 = m.m24;
    result.m43 = m.m34;
    result.m44 = m.m44;

    return result;
}

inline __host__ __device__ mat4 operator *(mat4 a, mat4 b);

inline __host__ __device__ mat4 make_rotation(float3 angles)
{
    mat4 result = identity_mat4();

    result = make_rotation_y(angles.y) * result;
    result = make_rotation_x(angles.x) * result;
    result = make_rotation_z(angles.z) * result;

    return result;
}

inline __host__ __device__ float4 operator *(mat4 m, float4 v)
{
    return make_float4(
        dot(v, make_float4(m.m11, m.m12, m.m13, m.m14)),
        dot(v, make_float4(m.m21, m.m22, m.m23, m.m24)),
        dot(v, make_float4(m.m31, m.m32, m.m33, m.m34)),
        dot(v, make_float4(m.m41, m.m42, m.m43, m.m44))
    );
}

inline __host__ __device__ float3 operator *(mat4 m, float3 v)
{
    return make_float3(
        dot(make_float4(v.x, v.y, v.z, 1.0), make_float4(m.m11, m.m12, m.m13, m.m14)),
        dot(make_float4(v.x, v.y, v.z, 1.0), make_float4(m.m21, m.m22, m.m23, m.m24)),
        dot(make_float4(v.x, v.y, v.z, 1.0), make_float4(m.m31, m.m32, m.m33, m.m34))
    );
}

inline __host__ __device__ mat4 operator *(mat4 a, mat4 b)
{
    float4 a1 = make_float4(a.m11, a.m12, a.m13, a.m14);
    float4 a2 = make_float4(a.m21, a.m22, a.m23, a.m24);
    float4 a3 = make_float4(a.m31, a.m32, a.m33, a.m34);
    float4 a4 = make_float4(a.m41, a.m42, a.m43, a.m44);
    float4 b1 = make_float4(b.m11, b.m21, b.m31, b.m41);
    float4 b2 = make_float4(b.m12, b.m22, b.m32, b.m42);
    float4 b3 = make_float4(b.m13, b.m23, b.m33, b.m43);
    float4 b4 = make_float4(b.m14, b.m24, b.m34, b.m44);


    return make_mat4(
        dot(a1, b1), dot(a1, b2), dot(a1, b3), dot(a1, b4),
        dot(a2, b1), dot(a2, b2), dot(a2, b3), dot(a2, b4),
        dot(a3, b1), dot(a3, b2), dot(a3, b3), dot(a3, b4),
        dot(a4, b1), dot(a4, b2), dot(a4, b3), dot(a4, b4)
    );
}



//==============================================================================
//  Camera functions
//==============================================================================
inline __host__ __device__ mat4 get_inv_proj(camera_t cam)
{
    float f = cam.far, n = cam.near;
    float t = tan(cam.fov / 180 * M_PI / 2) * n, b = -t;
    float r = t * cam.aspect, l = -r;

    mat4 inv_proj = zero_mat4();
    inv_proj.m11 = -f * (r - l) / (f - n);
    inv_proj.m14 = -f * (r + l) / (f - n);
    inv_proj.m22 = -f * (t - b) / (f - n);
    inv_proj.m24 = -f * (t + b) / (f - n);
    inv_proj.m34 = 2 * f * n / (f - n);
    inv_proj.m43 = 1;
    inv_proj.m44 = -(f + n) / (f - n);

    return inv_proj;
}

inline __host__ __device__ mat4 get_inv_view(camera_t cam)
{
    mat4 inv_view = identity_mat4();
    inv_view = transpose(make_rotation(-cam.rotation)) * inv_view;
    inv_view = make_translation(-cam.position) * inv_view;
    return inv_view;
}



//==============================================================================
//  Ray functions
//==============================================================================
inline __host__ __device__ bool intersection(ray_t r, sphere_t s, float* t)
{
    float3 L = s.center - r.orig;
    float tca = dot(L, r.dir);
    float d2 = dot(L, L) - tca * tca;
    float r2 = s.radius * s.radius;

    if (dot(L, L) < r2) {
        *t = 0;
        return true;
    }

    if (tca < 0 || d2 > r2) {
        return false;
    }

    float thc = sqrt(r2 - d2);
    float t0 = tca - thc;
    float t1 = tca + thc;

    *t = fminf(t0, t1);

    return true;
}

inline __host__ __device__ bool intersection(ray_t r, plane_t p, float* t)
{
    float denom = dot(r.dir, p.normal);

    if (abs(denom) < 0.0001f) {
        return false;
    }

    *t = dot(p.center - r.orig, p.normal) / denom;
    if (*t >= 0) {
        return true;
    }

    return false;
}

inline __host__ __device__ ray_t transform_ray(ray_t ray, float3 translation, float3 rotation)
{
    float4 dir = make_rotation(-rotation) * make_float4(ray.dir.x, ray.dir.y, ray.dir.z, 1.0);
    float4 orig =
        make_rotation(-rotation) *
        make_translation(translation) *
        make_float4(ray.orig.x, ray.orig.y, ray.orig.z, 1.0);

    ray.orig = make_float3(orig.x, orig.y, orig.z);
    ray.dir = make_float3(dir.x, dir.y, dir.z);
    return ray;
}



//==============================================================================
//  Other functions
//==============================================================================
