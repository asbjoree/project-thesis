#include <cuda.h>
#include <cuda_gl_interop.h>

#include "settings.h"
#include "math_helpers.h"
#include "scene.h"
#include "ray_tracer.h"


inline __device__ uint8_t get_first_node(float3 t0, float3 tm)
{
    uint8_t child = 0;

    if (t0.x > t0.y && t0.x > t0.z) {
        // YZ-plane
        child |= ((tm.y < t0.x) << 1);
        child |= ((tm.z < t0.x) << 2);
    } else if (t0.y > t0.x && t0.y > t0.z) {
        // XZ-plane
        child |= ((tm.x < t0.y) << 0);
        child |= ((tm.z < t0.y) << 2);
    } else {
        // XY-plane
        child |= ((tm.x < t0.z) << 0);
        child |= ((tm.y < t0.z) << 1);
    }
    return child;
}


inline __device__ void get_child_entry_coords(uint8_t child, float3 t0, float3 tm, float3 t1, float3* t0_next, float3* t1_next)
{
    t0_next->x = (child & 1) ? tm.x : t0.x;
    t0_next->y = (child & 2) ? tm.y : t0.y;
    t0_next->z = (child & 4) ? tm.z : t0.z;

    t1_next->x = (child & 1) ? t1.x : tm.x;
    t1_next->y = (child & 2) ? t1.y : tm.y;
    t1_next->z = (child & 4) ? t1.z : tm.z;
}


inline __device__ uint8_t get_next_child(uint8_t cur_child, float3 tm, float3 t1)
{
    float3 vec;
    vec.x = (cur_child & 1) ? t1.x : tm.x;
    vec.y = (cur_child & 2) ? t1.y : tm.y;
    vec.z = (cur_child & 4) ? t1.z : tm.z;
    uint3 next;
    next.x = (cur_child & 1) ? 8 : cur_child + 1;
    next.y = (cur_child & 2) ? 8 : cur_child + 2;
    next.z = (cur_child & 4) ? 8 : cur_child + 4;

    if (vec.x < vec.y && vec.x < vec.z) {
        return next.x;
    } else if (vec.y < vec.x && vec.y < vec.z) {
        return next.y;
    } else {
        return next.z;
    }
}


typedef struct {
    svo_entry_t* entry;
    float3 t0;
    float3 t1;
    float3 tm;
    uint8_t cur_child;

} traversal_stack_frame;

inline __device__ bool traverse_svo(
        svo_t svo,
        float* t_hit,
        uint8_t a,
        traversal_stack_frame* stack,
        uint8_t stack_idx,
        float4* hit_color,
        float3* hit_normal
        )
{
    if (stack[stack_idx].t1.x < 0.0 ||
        stack[stack_idx].t1.y < 0.0 ||
        stack[stack_idx].t1.z < 0.0
        ) {
        return false;
    }

    while (stack_idx < MAX_STACK_DEPTH) {

#ifdef SHOW_SVO_BOUNDARIES
        *hit_color *= 0.95;
#endif

        uint8_t child_transformed = a ^ stack[stack_idx].cur_child;
        float t_enter = fmaxf(fmaxf(stack[stack_idx].t0.x, stack[stack_idx].t0.y), stack[stack_idx].t0.z);
        float t_exit = fminf(fminf(stack[stack_idx].t1.x, stack[stack_idx].t1.y), stack[stack_idx].t1.z);

        if (t_exit > 0.0 && get_valid_mask(*stack[stack_idx].entry) & (1 << child_transformed)) {
            float3 t0_next, t1_next;
            get_child_entry_coords(
                    stack[stack_idx].cur_child,
                    stack[stack_idx].t0,
                    stack[stack_idx].tm,
                    stack[stack_idx].t1,
                    &t0_next,
                    &t1_next
                    );

            if (get_leaf_mask(*stack[stack_idx].entry) & (1 << child_transformed)) {
                //*hit_normal = get_normal_entry(stack[stack_idx].entry, child_transformed);
                *t_hit = fmaxf(fmaxf(t0_next.x, t0_next.y), t0_next.z);
                if (*t_hit == t0_next.x) {
                    *hit_normal = make_float3(1.0, 0.0, 0.0);
                } else if (*t_hit == t0_next.y) {
                    *hit_normal = make_float3(0.0, 1.0, 0.0);
                } else {
                    *hit_normal = make_float3(0.0, 0.0, 1.0);
                }
                *hit_color = make_float4(1.0, 1.0, 1.0, 1.0);
                return true;

            } else {
                stack_idx++;
                stack[stack_idx].entry = get_child_entry(stack[stack_idx - 1].entry, child_transformed);
                stack[stack_idx].t0 = t0_next;
                stack[stack_idx].t1 = t1_next;
                stack[stack_idx].tm = 0.5 * (t0_next + t1_next);
                stack[stack_idx].cur_child = get_first_node(stack[stack_idx].t0, stack[stack_idx].tm);
            }
        } else {
            uint8_t next_child = get_next_child(stack[stack_idx].cur_child, stack[stack_idx].tm, stack[stack_idx].t1);

            while (next_child == 8) {
                if (stack_idx == 0) {
                    return false;
                }

                stack_idx--;
                next_child = get_next_child(stack[stack_idx].cur_child, stack[stack_idx].tm, stack[stack_idx].t1);
            }

            stack[stack_idx].cur_child = next_child;
        }
    }

    return false;
}

inline __device__ void intersect_svo(ray_t ray, float scale, float3* t0, float3* t1, float* t_enter, float* t_exit, uint8_t* a)
{
    if (ray.dir.x == 0.0) {
        ray.dir.x = 0.0001;
    }
    if (ray.dir.y == 0.0) {
        ray.dir.y = 0.0001;
    }
    if (ray.dir.z == 0.0) {
        ray.dir.z = 0.0001;
    }

    *a = 0;
    float dim = scale * 0.5;

    if (ray.dir.x < 0.0) {
        ray.orig.x = -ray.orig.x;
        ray.dir.x = -ray.dir.x;
        *a |= 1;
    }

    if (ray.dir.y < 0.0) {
        ray.orig.y = -ray.orig.y;
        ray.dir.y = -ray.dir.y;
        *a |= 2;
    }

    if (ray.dir.z < 0.0) {
        ray.orig.z = -ray.orig.z;
        ray.dir.z = -ray.dir.z;
        *a |= 4;
    }

    *t0 = make_float3(
            (-dim - ray.orig.x) / ray.dir.x,
            (-dim - ray.orig.y) / ray.dir.y,
            (-dim - ray.orig.z) / ray.dir.z
            );

    *t1 = make_float3(
            (dim - ray.orig.x) / ray.dir.x,
            (dim - ray.orig.y) / ray.dir.y,
            (dim - ray.orig.z) / ray.dir.z
            );

    *t_enter = fmaxf(fmaxf(t0->x, t0->y), t0->z);
    *t_exit = fminf(fminf(t1->x, t1->y), t1->z);
}

inline __device__ bool trace_single_ray(svo_t svo, ray_t ray, float* t_hit, float4* hit_color, float3* hit_normal)
{
    float3 t0, t1;
    float t_enter, t_exit;
    uint8_t a;

    intersect_svo(ray, svo.scale, &t0, &t1, &t_enter, &t_exit, &a);

    if (t_enter < t_exit) {
        traversal_stack_frame stack[MAX_STACK_DEPTH] = {0};
        stack[0].entry = svo.entries;
        stack[0].t0 = t0;
        stack[0].t1 = t1;
        stack[0].tm = 0.5 * (t0 + t1);
        stack[0].cur_child = get_first_node(stack[0].t0, stack[0].tm);

        return traverse_svo(svo, t_hit, a, stack, 0, hit_color, hit_normal);
    }

    return false;
}

inline __device__ float4 calc_color(float4 color, float3 light_dir, float3 rotation, float3 normal)
{
    return color * abs(dot(light_dir, /*transpose(make_rotation(rotation)) * */normal));
}

inline __device__ void trace(scene_t scene, ray_t ray, hit_t* hit, camera_t camera, bool optimise)
{
    float4 color = CLEARCOL;
    float3 normal;
    float t_hit;

    hit_t new_hit;

    new_hit.type = HIT_NONE;
    new_hit.color = color;
    new_hit.t = 100000000.0;

    if (!optimise) {
        for (uint8_t idx = 0; idx < scene.svo_count; idx++) {
            ray_t ray_tr = transform_ray(ray, scene.svo[idx].position, scene.svo[idx].rotation);
            if (trace_single_ray(scene.svo[idx], ray_tr, &t_hit, &color, &normal)) {
                if (t_hit < new_hit.t) {
                    new_hit.t = t_hit;
                    new_hit.color = calc_color(color, LIGHT_DIR, scene.svo[idx].rotation, normal);
                    new_hit.normal = normal;
                }
            }
        }
    } else {
        for (uint8_t idx = 0; idx < scene.svo_count; idx++) {
            if (intersection(ray, get_bounding_sphere(scene.svo[idx]), &t_hit)) {
                if (new_hit.type == HIT_NONE) {
                    new_hit.type = HIT_SIMPLE;
                } else {
                    new_hit.type = HIT_MULTIPLE;
                }

                if (t_hit < new_hit.t) {
                    new_hit.t = t_hit;
                    new_hit.svo_idx = idx;
                }

            }
        }

        if (new_hit.type == HIT_SIMPLE && new_hit.svo_idx == hit->svo_idx && !scene.svo[hit->svo_idx].dirty && !camera.dirty) {
            return;
        }

        new_hit.t = 100000000.0;

        if (new_hit.type == HIT_SIMPLE) {
            ray_t ray_tr = transform_ray(ray, scene.svo[new_hit.svo_idx].position, scene.svo[new_hit.svo_idx].rotation);
            if (trace_single_ray(scene.svo[new_hit.svo_idx], ray_tr, &t_hit, &color, &normal)) {
                new_hit.t = t_hit;
                new_hit.color = calc_color(color, LIGHT_DIR, scene.svo[new_hit.svo_idx].rotation, normal);
                new_hit.normal = normal;
            }
        } else if (new_hit.type == HIT_MULTIPLE) {
            ray_t ray_tr = transform_ray(ray, scene.svo[new_hit.svo_idx].position, scene.svo[new_hit.svo_idx].rotation);
            if (trace_single_ray(scene.svo[new_hit.svo_idx], ray_tr, &t_hit, &color, &normal)) {
                new_hit.t = t_hit;
                new_hit.color = calc_color(color, LIGHT_DIR, scene.svo[new_hit.svo_idx].rotation, normal);
                new_hit.normal = normal;
            }

            for (uint8_t idx = 0; idx < scene.svo_count; idx++) {
                if (idx == new_hit.svo_idx) {
                    continue;
                }

                if (intersection(ray, get_bounding_sphere(scene.svo[idx]), &t_hit)) {
                    if (new_hit.t < t_hit) {
                        continue;
                    }
                    ray_tr = transform_ray(ray, scene.svo[idx].position, scene.svo[idx].rotation);
                    if (trace_single_ray(scene.svo[idx], ray_tr, &t_hit, &color, &normal)) {
                        if (t_hit < new_hit.t) {
                            new_hit.t = t_hit;
                            new_hit.color = calc_color(color, LIGHT_DIR, scene.svo[idx].rotation, normal);
                            new_hit.normal = normal;
                        }
                    }
                }
            }
        }
    }
    *hit = new_hit;
}

inline __device__ float2 get_sample_coords(uint32_t x, uint32_t y, uint32_t sample, uint32_t sampledim)
{
    float2 sample_offset = make_float2(
        (float)(sample % sampledim) / sampledim,
        (float)(sample / sampledim) / sampledim
    );

    float2 scr_coords = make_float2(
        (float)x + (sample_offset.x - 0.5) + 0.5 / sampledim,
        (float)y + (sample_offset.y - 0.5) + 0.5 / sampledim
    );

    return make_float2(scr_coords.x / (float)W_WIDTH * 2 - 1.0, scr_coords.y / (float)W_HEIGHT * 2 - 1.0);
}

inline __device__ __host__ ray_t generate_primary_ray(float2 coords, mat4 inv_proj, mat4 inv_view)
{
    float4 v0 = make_float4(coords.x, coords.y, -1, 1);
    float4 v1 = make_float4(coords.x, coords.y, 1, 1);

    float4 q0 = inv_proj * v0;
    float4 q1 = inv_proj * v1;
    q0 = q0 / q0.w;
    q1 = q1 / q1.w;

    q0 = inv_view * q0;
    q1 = inv_view * q1;

    ray_t generated_ray;
    generated_ray.orig = make_float3(q0.x, q0.y, q0.z);
    generated_ray.dir = normalize(make_float3(q1.x - q0.x, q1.y - q0.y, q1.z - q0.z));

    return generated_ray;
}

__global__ void tracing_kernel(
    float* pbo_ptr,
    hit_t* hit_buffer,
    double time,
    camera_t camera,
    scene_t scene,
    uint32_t sample,
    uint32_t msaa,
    bool optimise)
{
    int32_t x = blockIdx.x * blockDim.x + threadIdx.x;
    int32_t y = blockIdx.y * blockDim.y + threadIdx.y;

    if (x >= W_WIDTH || y >= W_HEIGHT) {
        return;
    }

    float4 color = make_float4(0.0, 0.0, 0.0, 0.0);
    uint32_t idx = (W_HEIGHT - y - 1) * W_WIDTH + x;
    uint32_t samplecount = msaa * msaa;
    uint32_t sample_idx = idx * samplecount + sample;


    if (sample != 0) {
        color.x = pbo_ptr[idx * 4 + 0];
        color.y = pbo_ptr[idx * 4 + 1];
        color.z = pbo_ptr[idx * 4 + 2];
        color.w = pbo_ptr[idx * 4 + 3];
    }

    float2 coords = get_sample_coords(x, y, sample, msaa);
    ray_t ray = generate_primary_ray(coords, get_inv_proj(camera), get_inv_view(camera));
    trace(scene, ray, &hit_buffer[sample_idx], camera, optimise);
    color = color + hit_buffer[sample_idx].color / samplecount;

    pbo_ptr[idx * 4 + 0] = color.x;
    pbo_ptr[idx * 4 + 1] = color.y;
    pbo_ptr[idx * 4 + 2] = color.z;
    pbo_ptr[idx * 4 + 3] = color.w;
}

