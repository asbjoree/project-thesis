#pragma once

enum hit_type_t {
    HIT_NONE,
    HIT_BB,
    HIT_SIMPLE,
    HIT_MULTIPLE
};

typedef struct {
    hit_type_t type;
    int8_t svo_idx;
    float t;
    float4 color;
    float3 normal;
} hit_t;

__global__ void tracing_kernel(
    float* pbo_ptr,
    hit_t* hit_buffer,
    double time,
    camera_t camera,
    scene_t scene,
    uint32_t sample,
    uint32_t msaa,
    bool optimise
);
