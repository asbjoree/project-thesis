#include <GL/glew.h>
#include <cuda.h>
#include <cuda_gl_interop.h>
#include <stdio.h>

#include "settings.h"
#include "math_helpers.h"
#include "scene.h"
#include "ray_tracer.h"
#include "animation.h"

cudaGraphicsResource_t pbo_resource;
void* pbo_ptr;
size_t pbo_size;

hit_t* hit_buffer;

dim3 threadsPerBlock;
dim3 numBlocks;

camera_t camera;
scene_t scene;

extern "C" void setupCuda(uint32_t pbo_buffer_gl, state_info_t* sInfo)
{

    cudaError_t err = cudaGraphicsGLRegisterBuffer(&pbo_resource, pbo_buffer_gl, cudaGraphicsRegisterFlagsWriteDiscard);
    if (err) {
        printf("Error registering CUDA resource!\n");
        return;
    }

    cudaGraphicsMapResources(1, &pbo_resource, 0);
    cudaGraphicsResourceGetMappedPointer(&pbo_ptr, &pbo_size, pbo_resource);
    if (sInfo->verbose) {
        printf("Successfully mapped CUDA PBO buffer of size %d.\n", pbo_size);
    }

    threadsPerBlock = dim3(24, 24);
    numBlocks = dim3(
        (int)ceil((float)W_WIDTH / threadsPerBlock.x),
        (int)ceil((float)W_HEIGHT / threadsPerBlock.y)
    );
    if (sInfo->verbose) {
        printf("CUDA blocks: %dx%d\n", numBlocks.x, numBlocks.y);
    }


    uint32_t hit_buffer_size = W_WIDTH * W_HEIGHT * sInfo->msaa * sInfo->msaa;
    hit_t* hit_buffer_h = (hit_t*)malloc(hit_buffer_size * sizeof(hit_t));

    for (uint32_t i = 0; i < hit_buffer_size; i++) {
        hit_buffer_h[i].type = HIT_NONE;
        hit_buffer_h[i].color = make_float4(0.0, 1.0, 0.0, 1.0);
        hit_buffer_h[i].normal = make_float3(1.0, 0.0, 0.0);
        hit_buffer_h[i].svo_idx = -1;
    }

    cudaMalloc(&hit_buffer, hit_buffer_size * sizeof(hit_t));
    cudaMemcpy(hit_buffer, hit_buffer_h, hit_buffer_size * sizeof(hit_t), cudaMemcpyHostToDevice);
    free(hit_buffer_h);

    camera.far = 100.0;
    camera.near = 0.001;
    camera.fov = 75.0;
    camera.aspect = (float)W_WIDTH / W_HEIGHT;
    //camera.rotation.y = M_PI * 1.07;
    camera.rotation.y = -M_PI * 1.14;
    camera.rotation.x = -M_PI * 0.16;
    //camera.position = make_float3(-0.17, 0.27, -0.6);
    camera.position = make_float3(0.37, 0.27, -0.6);
    camera.dirty = false;

    generate_scene(&scene, sInfo);
}

inline bool cameraDirty(camera_t lastCam)
{
    if (camera.far != lastCam.far) return true;
    if (camera.near != lastCam.near) return true;
    if (camera.fov != lastCam.fov) return true;
    if (camera.aspect != lastCam.aspect) return true;
    if (camera.rotation.x != lastCam.rotation.x) return true;
    if (camera.rotation.y != lastCam.rotation.y) return true;
    if (camera.rotation.z != lastCam.rotation.z) return true;
    if (camera.position.x != lastCam.position.x) return true;
    if (camera.position.y != lastCam.position.y) return true;
    if (camera.position.z != lastCam.position.z) return true;
    return false;
}

extern "C" void runCuda(state_info_t* sInfo)
{
    double time = sInfo->time;
    if (sInfo->screenshots) {
        time = (double)sInfo->frameCnt / 60;
    }

    if (sInfo->animation) {
        animation_kernel<<<1, 1>>>(scene, time);
    }

    static camera_t lastCamera;

    if (cameraDirty(lastCamera)) {
        lastCamera = camera;
        lastCamera.dirty = true;
    } else {
        lastCamera = camera;
        lastCamera.dirty = false;
    }

    for (uint32_t sample = 0; sample < sInfo->msaa * sInfo->msaa; sample++) {
        tracing_kernel<<<numBlocks, threadsPerBlock>>>(
            (float*)pbo_ptr,
            hit_buffer,
            time,
            lastCamera,
            scene,
            sample,
            sInfo->msaa,
            sInfo->optimise
        );
        cudaDeviceSynchronize();
    }
    camera.dirty = false;
}

extern "C" void updateCamera(float dx, float dy, float dz, float db, float de)
{
    if (dx == 0.0 && dy == 0.0 && dz == 0.0 && db == 0.0 && de == 0.0) {
        return;
    }

    camera.rotation.y = camera.rotation.y + db;
    camera.rotation.x = min(max(camera.rotation.x + de, -M_PI/2), M_PI/2);

    float4 dp = transpose(make_rotation(-camera.rotation)) * make_float4(dx, dy, dz, 1.0);
    camera.position.x += dp.x;
    camera.position.y += dp.y;
    camera.position.z += dp.z;
}

extern "C" void destroyCuda()
{
    cudaGraphicsUnregisterResource(pbo_resource);
}

