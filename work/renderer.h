#pragma once

void setupCuda(uint32_t pbo_buffer_gl, state_info_t* sInfo);
void runCuda(state_info_t* sInfo);
void updateCamera(float dx, float dy, float dz, float db, float de);
void destroyCuda();
