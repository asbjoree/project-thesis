#include "settings.h"
#include "state_info.h"
#include "scene.h"

void generate_scene(scene_t* scene, state_info_t* sInfo)
{
    scene_t scene_h;
    //generate_dummy_svo(&(scene_h.svo[0]));
    //load_svo((char*)"models/svo/sphere.svo", &(scene_h.svo[0]));
    if (sInfo->animation) {
        scene_h.svo_count = 8;
        scene_h.svo = (svo_t*)malloc(scene_h.svo_count * sizeof(svo_t));
        load_svo((char*)"models/svo/car_body_2048.svo", &(scene_h.svo[0]), sInfo);
        load_svo((char*)"models/svo/car_wheel_512.svo", &(scene_h.svo[1]), sInfo);
        load_svo((char*)"models/svo/car_wheel_512.svo", &(scene_h.svo[2]), sInfo);
        load_svo((char*)"models/svo/car_wheel_512.svo", &(scene_h.svo[3]), sInfo);
        load_svo((char*)"models/svo/car_wheel_512.svo", &(scene_h.svo[4]), sInfo);
        load_svo((char*)"models/svo/car_door_left_512.svo", &(scene_h.svo[5]), sInfo);
        load_svo((char*)"models/svo/car_door_right_512.svo", &(scene_h.svo[6]), sInfo);
        load_svo((char*)"models/svo/car_steering_wheel_512.svo", &(scene_h.svo[7]), sInfo);
    } else {
        scene_h.svo_count = 1;
        scene_h.svo = (svo_t*)malloc(scene_h.svo_count * sizeof(svo_t));
        load_svo((char*)"models/svo/car_full_2048.svo", &(scene_h.svo[0]), sInfo);
    }

    scene->svo_count = scene_h.svo_count;

    cudaMalloc(&scene->svo, scene->svo_count * sizeof(svo_t));
    cudaMemcpy(scene->svo, scene_h.svo, scene->svo_count * sizeof(svo_t), cudaMemcpyHostToDevice);

    free(scene_h.svo);
}

void free_scene(scene_t scene)
{
    for (uint32_t i = 0; i < scene.svo_count; i++) {
        free_svo(scene.svo[i]);
    }

    cudaFree(scene.svo);
}
