#include "state_info.h"
#include "svo.h"

typedef struct {
    svo_t* svo;
    uint32_t svo_count;
} scene_t;

void generate_scene(scene_t* scene, state_info_t* sInfo);
void free_scene(scene_t scene);
