#pragma once

#define W_WIDTH 1280
#define W_HEIGHT 768

#define MOVEMENT_SPEED 0.03
#define MOVEMENT_FORWARD GLFW_KEY_E
#define MOVEMENT_BACKWARD GLFW_KEY_D
#define MOVEMENT_LEFT GLFW_KEY_S
#define MOVEMENT_RIGHT GLFW_KEY_F
#define MOVEMENT_UP GLFW_KEY_SPACE
#define MOVEMENT_DOWN GLFW_KEY_LEFT_SHIFT
#define MOUSE_SENSITIVITY 0.005

#define MAX_STACK_DEPTH 16
//#define SHOW_SVO_BOUNDARIES 1
#define CLEARCOL make_float4(1.0, 1.0, 1.0, 1.0);
#define LIGHT_DIR make_float3(0.566, 0.707, 0.424)
