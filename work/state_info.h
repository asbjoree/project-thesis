#pragma once

#include <stdint.h>
#include <stdbool.h>

typedef struct {
    uint32_t frameCnt;
    double time;
    bool verbose;
    bool animation;
    bool optimise;
    bool movement;
    bool screenshots;
    bool fps;
    uint8_t msaa;
} state_info_t;
