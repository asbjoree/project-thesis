#include "state_info.h"
#include "svo.h"
#include "utilities.h"

__device__ bool get_is_far(svo_entry_t entry)
{
    return entry >> 31;
}

__device__ uint16_t get_child_table_pointer(svo_entry_t entry)
{
    return (entry >> 16) & 0x7FFF;
}

__device__ uint8_t get_valid_mask(svo_entry_t entry)
{
    return (entry >> 8) & 0xFF;
}

__device__ uint8_t get_leaf_mask(svo_entry_t entry)
{
    return entry & 0xFF;
}

__device__ svo_entry_t* get_child_entry(svo_entry_t* entry, uint8_t idx)
{
    svo_entry_t* child_ptr = entry + get_child_table_pointer(*entry);

    if (get_is_far(*entry)) {
        child_ptr = entry + (uint32_t)(*child_ptr);
    }

    uint8_t valid_mask = get_valid_mask(*entry);
    uint8_t leaf_mask = get_leaf_mask(*entry);

    uint32_t offset = 0;
    for (uint8_t i = 0; i < idx; i++) {
        if ((valid_mask >> i) & 0x01) {
            child_ptr++;
        }
    }

    return child_ptr + offset;
}

__device__ float3 get_normal_entry(svo_entry_t* entry, uint8_t idx)
{
    svo_entry_t* child_ptr = entry + get_child_table_pointer(*entry);

    if (get_is_far(*entry)) {
        child_ptr = entry + (uint32_t)(*child_ptr);
    }

    uint8_t valid_mask = get_valid_mask(*entry);
    uint8_t leaf_mask = get_leaf_mask(*entry);

    uint32_t offset = 0;
    for (uint8_t i = 0; i < idx; i++) {
        if ((valid_mask >> i) & 0x01) {
            child_ptr++;
        }
    }

    uint32_t val = *(child_ptr + offset);

    return make_float3((float)(val & 0xFF) / 255, (float)((val >> 8) & 0xFF) / 255, (float)((val >> 16) & 0xFF) / 255);
}

__device__ sphere_t get_bounding_sphere(svo_t svo)
{
    sphere_t sphere;
    sphere.center = svo.position;
    sphere.radius = sqrt(svo.scale * svo.scale * 0.25 * 3);
    return sphere;
}


inline uint32_t get_svo_entry(
        bool is_far,
        uint16_t child_table_pointer,
        uint8_t valid_mask,
        uint8_t leaf_mask
        )
{
    uint32_t entry = 0;
    entry |= is_far ? (1 << 31) : 0;
    entry |= (uint32_t)(child_table_pointer & 0x7FFF) << 16;
    entry |= (uint32_t)(valid_mask) << 8;
    entry |= (uint32_t)leaf_mask;

    return entry;
}

void generate_dummy_svo(svo_t* svo)
{
    svo_t svo_h;
    svo_h.size = 10;
    svo_h.entries = (svo_entry_t*)malloc(svo_h.size * sizeof(svo_entry_t));
    svo_h.entries[0] = get_svo_entry(false, 0x1, 0x5A, 0x10);

    svo_h.entries[1] = get_svo_entry(false, 0x4, 0x14, 0x10);
    svo_h.entries[2] = get_svo_entry(true, 0x2, 0x19, 0x11);
    svo_h.entries[3] = get_svo_entry(false, 0x4, 0x2C, 0x00);

    svo_h.entries[4] = 0x04;

    svo_h.entries[5] = get_svo_entry(false, 0x0, 0x12, 0x12);

    svo_h.entries[6] = get_svo_entry(false, 0x0, 0x4C, 0x4C);

    svo_h.entries[7] = get_svo_entry(false, 0x0, 0x06, 0x06);
    svo_h.entries[8] = get_svo_entry(false, 0x0, 0x61, 0x61);
    svo_h.entries[9] = get_svo_entry(false, 0x0, 0x10, 0x10);

    svo->size = svo_h.size;
    svo->rotation = make_float3(0.0, 0.0, 0.0);
    svo->position = make_float3(0.0, 0.0, 0.0);
    svo->scale = 1.0;
    svo->dirty = true;

    cudaMalloc(&svo->entries, svo->size * sizeof(svo_entry_t));
    cudaMemcpy(svo->entries, svo_h.entries, svo->size * sizeof(svo_entry_t), cudaMemcpyHostToDevice);

    free(svo_h.entries);
}


void load_svo(char* filename, svo_t* svo, state_info_t* sInfo)
{
    uint32_t length;
    uint8_t* buffer;
    read_file(filename, &length, &buffer, sInfo);

    svo_t svo_h;
    svo_h.size = length / (sizeof(svo_entry_t) / sizeof(uint8_t));
    svo_h.entries = (svo_entry_t*)buffer;
    svo->size = svo_h.size;
    svo->rotation = make_float3(0.0, 0.0, 0.0);
    svo->position = make_float3(0.0, 0.0, 0.0);
    svo->scale = 1.0;
    svo->dirty = true;

    cudaMalloc(&svo->entries, svo->size * sizeof(svo_entry_t));
    cudaMemcpy(svo->entries, svo_h.entries, svo->size * sizeof(svo_entry_t), cudaMemcpyHostToDevice);

    free(svo_h.entries);
}

void free_svo(svo_t svo)
{
    cudaFree(svo.entries);
}

