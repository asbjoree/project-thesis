#pragma once
#include <stdio.h>
#include <stdint.h>

#include "math_helpers.h"

typedef uint32_t svo_entry_t;

typedef struct {
    svo_entry_t* entries;
    uint32_t size;
    float3 position;
    float3 rotation;
    float scale;
    bool dirty;
} svo_t;

enum svo_child_index {
    SVO_NONE = 0x00,
    SVO_C0 = 0x01,
    SVO_C1 = 0x02,
    SVO_C2 = 0x04,
    SVO_C3 = 0x08,
    SVO_C4 = 0x10,
    SVO_C5 = 0x20,
    SVO_C6 = 0x40,
    SVO_C7 = 0x80,
};

#ifdef __CUDACC__

__device__ uint8_t get_valid_mask(svo_entry_t entry);
__device__ uint8_t get_leaf_mask(svo_entry_t entry);
__device__ svo_entry_t* get_child_entry(svo_entry_t* entry, uint8_t idx);
__device__ float3 get_normal_entry(svo_entry_t* entry, uint8_t idx);
__device__ sphere_t get_bounding_sphere(svo_t svo);

#endif

void load_svo(char* filename, svo_t* svo, state_info_t* sInfo);
void generate_dummy_svo(svo_t* svo);
void free_svo(svo_t svo);

