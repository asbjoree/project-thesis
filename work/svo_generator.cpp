#include <stdio.h>
#include <stdint.h>
#include <queue>
#include <vector>
#include <algorithm>

#include "svo_generator.h"


inline uint32_t get_svo_entry(
        bool is_far,
        uint16_t child_table_pointer,
        uint8_t valid_mask,
        uint8_t leaf_mask
        )
{
    uint32_t entry = 0;
    entry |= is_far ? (1 << 31) : 0;
    entry |= (uint32_t)(child_table_pointer & 0x7FFF) << 16;
    entry |= (uint32_t)(valid_mask) << 8;
    entry |= (uint32_t)leaf_mask;

    return entry;
}

inline uint32_t get_svo_normal(uint32_t x, uint32_t y, uint32_t z, uint32_t dim)
{
    uint32_t entry = 0;
    entry |= (uint32_t)(((float)x / dim) * 255) & 0xFF;
    entry << 8;
    entry |= (uint32_t)(((float)y / dim) * 255) & 0xFF;
    entry << 8;
    entry |= (uint32_t)(((float)z / dim) * 255) & 0xFF;

    return entry;
}

class octree_node {
    public:
        uint32_t max_depth;
        uint32_t posx;
        uint32_t posy;
        uint32_t posz;
        uint32_t dim;
        uint8_t leaf_mask;
        uint8_t valid_mask;
        octree_node* children[8] = {nullptr};
        bool leaves[8] = {false};
        octree_node* parent = nullptr;

    octree_node(uint8_t* raw_data, uint32_t root_dim, uint32_t dim, uint32_t ox, uint32_t oy, uint32_t oz, octree_node* parent)
    {
        this->dim = dim;
        this->posx = ox;
        this->posy = oy;
        this->posz = oz;

        valid_mask = 0xFF;
        leaf_mask = 0x00;
        max_depth = 0;
        this->parent = parent;
        for (int dz = 0; dz < 2; dz++) {
            for (int dy = 0; dy < 2; dy++) {
                for (int dx = 0; dx < 2; dx++) {
                    uint32_t idx = dx + dy * 2 + dz * 2 * 2;
                    if (dim > 2) {
                        children[idx] = new octree_node(raw_data, root_dim, dim / 2, ox + dx * dim / 2, oy + dy * dim / 2, oz + dz * dim / 2, this);

                        if (children[idx]->leaf_mask == children[idx]->valid_mask) {
                            if (children[idx]->valid_mask == 0xFF) {
                                leaf_mask |= 1 << idx;
                                valid_mask |= 1 << idx;
                                delete children[idx];
                                children[idx] = nullptr;
                                leaves[idx] = true;
                            } else if (children[idx]->valid_mask == 0x00) {
                                leaf_mask &= ~(1 << idx);
                                valid_mask &= ~(1 << idx);
                                delete children[idx];
                                children[idx] = nullptr;
                                leaves[idx] = false;
                            }
                        }

                        if (children[idx] != nullptr && children[idx]->max_depth + 1 > max_depth) {
                            max_depth = children[idx]->max_depth + 1;
                        }
                    } else {
                        uint8_t unfilled = get_raw_value(raw_data, root_dim, ox + dx, oy + dy, oz + dz) == 0;
                        valid_mask &= ~(unfilled << idx);
                        leaf_mask = valid_mask;
                        leaves[idx] = true;
                    }
                }
            }
        }
    }

    ~octree_node()
    {
        for (int idx = 0; idx < 8; idx++) {
            if (children[idx] != nullptr) {
                delete children[idx];
            }
        }
    }

    uint32_t get_descendant_count()
    {
        int count = 1;
        for (int idx = 0; idx < 8; idx++) {
            if (children[idx] != nullptr) {
                count += children[idx]->get_descendant_count();
            }
        }
        return count;
    }

    uint32_t get_child_count()
    {
        int count = 0;
        for (int idx = 0; idx < 8; idx++) {
            if (children[idx] != nullptr) {
                count++;
            }
        }
        return count;
    }

    uint32_t get_leaf_count()
    {
        uint8_t count = 0;
        for (int idx = 0; idx < 8; idx++) {
            if (leaves[idx]) {
                count++;
            }
        }
        return count;
    }

    uint32_t get_depth()
    {
        if (parent != nullptr) {
            return parent->get_depth() + 1;
        }
        return 0;
    }

    uint8_t get_raw_value(uint8_t* raw_data, uint32_t dim, uint32_t x, uint32_t y, uint32_t z)
    {
        uint64_t idx = (uint64_t)x * (uint64_t)dim * (uint64_t)dim + (uint64_t)z * (uint64_t)dim + (uint64_t)y;
        return raw_data[idx];
    }

    octree_node* get_first_child()
    {
        for (int idx = 0; idx < 8; idx++) {
            if (children[idx] != nullptr) {
                return children[idx];
            }
        }

        return nullptr;
    }
};

void generate_svo_bank(std::vector<uint32_t>* bank_entries, octree_node* root)
{
    std::queue<octree_node*> bfs_queue;
    //std::queue<uint32_t> normal_queue;

    for (int idx = 0; idx < 8; idx++) {
        if (root->children[idx] != nullptr) {
            bfs_queue.push(root->children[idx]);
        }
        //if (root->leaves[idx]) {
        //    bfs_queue.push(nullptr);
        //}
    }

    while (!bfs_queue.empty()) {
        octree_node* node = bfs_queue.front();

        if (bfs_queue.size() > 0x7FFF) {
            printf("ERROR\n");
        }

        if (node == nullptr) {
            //bank_entries->push_back(normal_queue.front());
            //normal_queue.pop();
            bfs_queue.pop();
        } else {
            bank_entries->push_back(get_svo_entry(false, bfs_queue.size(), node->valid_mask, node->leaf_mask));
            bfs_queue.pop();

            for (int idx = 0; idx < 8; idx++) {
                if (node->children[idx] != nullptr) {
                    bfs_queue.push(node->children[idx]);
                }
                //if (node->leaves[idx]) {
                //    normal_queue.push(get_svo_normal(node->posx, node->posy, node->posz, node->dim));
                //    bfs_queue.push(nullptr);
                //}
            }
        }
    }
}

uint32_t svo_bank_depth(octree_node* root)
{
    if (root->get_descendant_count() < 0x1FFF) {
        return 0;
    }

    uint32_t max = 0;

    for (int idx = 0; idx < 8; idx++) {
        if (root->children[idx] != nullptr) {
            uint32_t cur = svo_bank_depth(root->children[idx]);
            if (cur > max) {
                max = cur;
            }
        }
    }

    return max + 1;
}

void generate_svo_entries(uint32_t* svo_size, uint32_t** svo_entries, octree_node* root)
{
    std::vector<uint32_t> entries;

    uint32_t bank_depth = svo_bank_depth(root);

    //printf("Bank depth: %d\n", bank_depth);

    if (bank_depth > 0) {
        std::queue<octree_node*> bfs_queue, bank_queue;
        bfs_queue.push(root);

        while (bfs_queue.size() > 0) {
            octree_node* node = bfs_queue.front();

            if (bfs_queue.size() > 0x7FFF) {
                printf("ERROR\n");
            }

            if (node->get_depth() < bank_depth) {
                entries.push_back(get_svo_entry(false, bfs_queue.size(), node->valid_mask, node->leaf_mask));
                for (int idx = 0; idx < 8; idx++) {
                    if (node->children[idx] != nullptr) {
                        bfs_queue.push(node->children[idx]);
                    }
                }
            } else {
                entries.push_back(get_svo_entry(true, bfs_queue.size() + bank_queue.size(), node->valid_mask, node->leaf_mask));
                //printf("Create bank for child with %d descendants, ptr: %d\n", node->get_descendant_count(), bfs_queue.size() + bank_queue.size());
                bank_queue.push(node);
            }

            bfs_queue.pop();
        }

        uint32_t number_of_banks = bank_queue.size();
        printf("Total number of banks: %d\n", number_of_banks);

        std::vector<uint32_t>::iterator far_ptrs = entries.end();
        std::vector<uint32_t> bank_entries;

        while (!bank_queue.empty()) {
            octree_node* node = bank_queue.front();
            uint32_t far_ptr = bank_queue.size() + number_of_banks + bank_entries.size();
            //printf("Child cnt: %d, Bank size: %d, Far ptr: %d\n", node->get_child_count(), bank_entries.size(), far_ptr);
            entries.push_back(far_ptr);
            bank_queue.pop();

            generate_svo_bank(&bank_entries, node);
        }

        entries.insert(entries.end(), bank_entries.begin(), bank_entries.end());
    } else {
        entries.push_back(get_svo_entry(false, 0x0001, root->valid_mask, root->leaf_mask));
        generate_svo_bank(&entries, root);
    }


    *svo_size = entries.size();
    *svo_entries = (uint32_t*)malloc(*svo_size * sizeof(uint32_t));
    std::copy(entries.begin(), entries.end(), *svo_entries);
}

int generate_svo(uint8_t* buffer, uint32_t dim, uint32_t* svo_size, uint32_t** svo_entries)
{
    octree_node* octree = new octree_node(buffer, dim, dim, 0, 0, 0, nullptr);

    generate_svo_entries(svo_size, svo_entries, octree);

    printf("Generated %d entries. Max depth: %d.\n", *svo_size, octree->max_depth);

    delete octree;
    return 0;
}
