#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "state_info.h"

int read_file(const char* filename, uint32_t* length, uint8_t** data, state_info_t* sInfo)
{
    FILE* fp = fopen(filename, "rb");
    if (fp == NULL) {
        printf("Error opening file '%s'\n", filename);
        return 1;
    }

    fseek(fp, 0, SEEK_END);
    int num = ftell(fp);
    rewind(fp);

    char* buffer = (char*)malloc(num * sizeof(char));
    fread(buffer, num, 1, fp);
    fclose(fp);

    *length = num;
    *data = (uint8_t*)buffer;

    if (sInfo->verbose) {
        printf("Loaded file '%s' (%d bytes)\n", filename, *length);
    }
    return 0;
}

int write_file(const char* filename, uint32_t length, uint8_t* data, state_info_t* sInfo)
{
    FILE* fp = fopen(filename, "wb");
    if (fp == NULL) {
        printf("Error opening file '%s'\n", filename);
        return 1;
    }

    fwrite((char*)data, 1, length, fp);
    fclose(fp);

    if (sInfo->verbose) {
        printf("Wrote file '%s' (%d bytes)\n", filename, length);
    }
    return 0;
}
