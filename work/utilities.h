#include <stdint.h>
#include "state_info.h"

extern "C" int read_file(const char* filename, uint32_t* length, uint8_t** data, state_info_t* sInfo);
extern "C" int write_file(const char* filename, uint32_t length, uint8_t* data, state_info_t* sInfo);
